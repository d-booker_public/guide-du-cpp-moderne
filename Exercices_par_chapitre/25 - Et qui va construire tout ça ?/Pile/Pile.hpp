#ifndef PILE_HPP
#define PILE_HPP

#include <vector>

class Pile
{
public:
  void ajouter(int valeur);
  void supprimer();
  // Pas de noexcept car en interne, std::vector ne fournit pas la
  // garantie que récupérer un élément ne lancera pas d'exception.
  int sommet() const;
  bool est_vide() const noexcept;
  std::size_t taille() const noexcept;

private:
  std::vector<int> m_pile;
};

#endif