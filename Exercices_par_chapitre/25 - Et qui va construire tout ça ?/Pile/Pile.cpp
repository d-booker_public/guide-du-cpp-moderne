#include "Pile.hpp"
#include <cassert>

void Pile::ajouter(int valeur)
{
  m_pile.push_back(valeur);
}

void Pile::supprimer()
{
  assert(m_pile.size() > 0 && "Impossible de supprimer un élément d'une pile vide."); 
  m_pile.pop_back();
}

int Pile::sommet() const
{
  assert(m_pile.size() > 0 && "Impossible de récupérer le sommet d'une pile vide."); 
  return m_pile.back();
}

bool Pile::est_vide() const noexcept
{
  return m_pile.empty();
}

std::size_t Pile::taille() const noexcept
{
  return m_pile.size();
}