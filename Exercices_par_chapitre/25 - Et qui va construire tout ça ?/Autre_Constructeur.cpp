Fraction::Fraction(std::string const & fraction)
{
  // Vérification que la chaîne n'est pas vide.
  assert(!std::empty(fraction) && "La chaîne ne peut pas être vide.");

  // Vérification de la présence du séparateur /.
  auto debut = std::begin(fraction);
  auto fin = std::end(fraction);
  assert(std::count(debut, fin, '/') == 1 && "Il ne doit y avoir qu'un seul séparateur '/'.");
  
  auto iterateur = std::find(debut, fin, '/');
  // Récupération du numérateur.
  std::string const n { debut, debut + iterateur };
  assert(!std::empty(n) && "Le numérateur doit être renseigné.");
  m_numerateur = std::stoi(n);

  // Pour sauter le caractère /.
  ++iterateur;

  // Récupération du dénominateur.
  std::string const d { iterateur, fin };
  assert(!std::empty(d) && "Le dénominateur doit être renseigné.");

  m_denominateur = std::stoi(d);
  assert(m_denominateur != 0 && "Le dénominateur ne peut pas être nul.");
}