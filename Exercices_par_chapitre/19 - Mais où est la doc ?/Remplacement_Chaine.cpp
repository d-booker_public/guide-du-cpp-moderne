#include <cassert> 
#include <iostream> 
#include <string> 
 
int main() 
{ 
  std::string phrase { "J'aimerais annuler ce texte." }; 
  std::string const mot { "remplacer" }; 
 
  // Le code. 
  phrase.replace(10, 7, mot); 
 
  std::cout << phrase << std::endl; 
  assert(phrase == "J'aimerais remplacer ce texte." && "Le texte n'a pas été modifié correctement."); 
  return 0; 
}