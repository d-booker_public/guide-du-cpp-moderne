#include <iostream>

int main() 
{ 
  std::cout << "Voici un exemple sans échappement : " << "'" << " est bien affiché." << std::endl; 
  std::cout << "Maintenant, je vais l'échapper : " << '\'' << " est bien affiché grâce à l'échappement." << std::endl; 
  
  std::cout << "La suite, après la tabulation : \tJe suis loin." << std::endl; 
  std::cout << "Ces mots sont sur une ligne.\nEt ceux là sur la suivante.\n"; 
  
  return 0; 
}
