#include <iostream> 
 
int main() 
{ 
  int nombre { 1 }; 
  int const multiplicateur { 2 };
  nombre = nombre * multiplicateur; 
  std::cout << "Maintenant, mon nombre vaut " << nombre << std::endl;
  
  nombre = nombre * multiplicateur; 
  std::cout << "Maintenant, mon nombre vaut " << nombre << std::endl; 
  nombre = nombre * multiplicateur; 
  std::cout << "Maintenant, mon nombre vaut " << nombre << std::endl; 

  return 0; 
}