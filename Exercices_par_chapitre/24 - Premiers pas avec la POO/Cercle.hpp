#ifndef CERCLE_HPP
#define CERCLE_HPP

#include "Point.hpp"
#include <cmath>

struct Cercle
{
  double aire() const noexcept;
  double perimetre() const noexcept;
  int rayon;
  Point centre;
};

bool appartient(Cercle const & cercle, Point const & point);

#endif