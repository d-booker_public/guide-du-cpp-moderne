#include "Cercle.hpp"

// Avec C++17 et inférieur.
double const pi { std::acos(-1) };

// Si vous compilez avec C++20, vous pouvez utiliser
// le fichier <numbers>.
//
// using namespace std::numbers;
// La valeur pi est maintenant accessible.

double Cercle::aire() const noexcept
{
  return rayon * rayon * pi;
}

double Cercle::perimetre() const noexcept
{
  return 2 * rayon * pi;
}

bool appartient(Cercle const & cercle, Point const & point)
{
  return distance(cercle.centre, point) <= cercle.rayon; 
}