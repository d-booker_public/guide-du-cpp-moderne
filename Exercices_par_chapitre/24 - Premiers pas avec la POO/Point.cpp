#include "Point.hpp"
#include <cmath>

double distance(Point const & lhs, Point const & rhs)
{
  return std::sqrt(std::pow(lhs.x - rhs.x, 2) + std::pow(lhs.y - rhs.y, 2));
}