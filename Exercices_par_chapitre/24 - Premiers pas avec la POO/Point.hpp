#ifndef POINT_HPP
#define POINT_HPP

struct Point
{
  int x;
  int y;
};

double distance(Point const & lhs, Point const & rhs);

#endif