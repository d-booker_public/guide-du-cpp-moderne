#include <iostream> 
#include <vector> 

std::vector<int> distributeur(int total, std::vector<int> coupures_disponibles) 
{ 
  std::vector<int> resultat {}; 
 
  for (auto coupure : coupures_disponibles) 
  { 
    resultat.push_back(total / coupure); 
    total %= coupure; 
  } 
 
  return resultat; 
} 
 
int main() 
{ 
  std::vector<int> const coupures_disponibles { 500, 200, 100, 50, 20, 10, 5, 2, 1 }; 
  auto const coupures { distributeur(285, coupures_disponibles) }; 
   
  for (auto coupure : coupures) 
  { 
    std::cout << coupure << std::endl; 
  } 
 
  return 0; 
}