#include <iostream>

int main()
{
  std::cout << "Quel âge as-tu ? ";
  unsigned int age {0};
  std::cin >> age;

  if (age < 5u)
  {
    std::cout << "Tu n'es pas un peu jeune pour naviguer sur internet ? " << std::endl;
  }
  else if (age < 18u)
  {
    std::cout << "Tu es mineur !" << std::endl;
  }
  else if (age < 30u)
  {
    std::cout << "Vous êtes majeur !" << std::endl;
  }
  else if (age < 50u)
  {
    std::cout << "Vous n'êtes plus très jeunes, mais vous n'êtes pas encore vieux." << std::endl;
  }
  else if (age < 70u)
  {
    std::cout << "Vous commencez à prendre de l'âge." << std::endl;
  }
  else if (age < 120u)
  {
    std::cout << "Vous avez dû en voir, des choses, durant votre vie !" << std::endl;
  }
  else
  {
    std::cout << "Quelle longévité !" << std::endl;
  }

  return 0;
}