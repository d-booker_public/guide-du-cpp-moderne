#include <fstream> 
#include <iostream> 
#include <limits> 
#include <string> 
 
template<typename T> 
void entree_securisee(T & variable) 
{ 
  while (!(std::cin >> variable)) 
  { 
    std::cout << "Entrée invalide. Recommence." << std::endl; 
    std::cin.clear(); 
   
 std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); 
} 
} 
 
template <typename T, typename Predicat> 
void entree_securisee(T & variable, Predicat predicat) 
{ 
  while (!(std::cin >> variable) || !predicat(variable)) 
  { 
    std::cout << "Entrée invalide. Recommence." << std::endl; 
    std::cin.clear(); 
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); 
  } 
} 
 
void demander_infos(std::string & nom, std::string & prenom, std::string & sexe, int & age) 
{ 
  std::cout << "Nom ? "; 
  entree_securisee(nom); 
  std::cout << "Prénom ? "; 
  entree_securisee(prenom); 
  std::cout << "Sexe ? "; 
  entree_securisee(sexe); 
  std::cout << "Age ? "; 
  entree_securisee(age, [](int & age) { return age >= 0; }); 
} 
 
std::string enregistrer_infos(std::string const & nom, std::string const & prenom, std::string const & sexe, int age) 
{ 
  std::string nom_fichier { prenom + "." + nom + ".csv" }; 
  std::ofstream fichier { nom_fichier }; 
  fichier << nom << ',' << prenom << ',' << sexe << ',' << age;
  
  return nom_fichier; 
} 
 
int main() 
{ 
  std::string nom { "" }; 
  std::string prenom { "" }; 
  std::string sexe { "" }; 
  int age { 0 }; 
 
  demander_infos(nom, prenom, sexe, age); 
  auto nom_fichier = enregistrer_infos(nom, prenom, sexe, age); 
  std::cout << std::endl << "Ces données seront enregistrées dans le fichier " << nom_fichier << std::endl; 
 
  return 0; 
}