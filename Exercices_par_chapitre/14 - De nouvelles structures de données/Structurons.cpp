#include <fstream> 
#include <iostream> 
#include <limits> 
#include <string> 
 
template<typename T> 
void entree_securisee(T & variable) 
{ 
  while (!(std::cin >> variable)) 
  { 
    std::cout << "Entrée invalide. Recommence." << std::endl; 
    std::cin.clear(); 
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); 
  } 
} 
 
template <typename T, typename Predicat> 
void entree_securisee(T & variable, Predicat predicat) 
{ 
  while (!(std::cin >> variable) || !predicat(variable)) 
  { 
    std::cout << "Entrée invalide. Recommence." << std::endl; 
    std::cin.clear(); 
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); 
  } 
} 
 
struct InformationsPersonnelles 
{ 
  std::string prenom; 
  std::string nom; 
  std::string sexe; 
  int age; 
}; 
 
InformationsPersonnelles demander_infos() 
{ 
  InformationsPersonnelles infos {}; 
  std::cout << "Nom ? "; 
  entree_securisee(infos.nom); 
  std::cout << "Prénom ? "; 
  entree_securisee(infos.prenom); 
  std::cout << "Sexe ? "; 
  entree_securisee(infos.sexe); 
  std::cout << "Age ? "; 
  entree_securisee(infos.age, [](int & age) { return age >= 0; }); 
 
  return infos; 
} 
 
std::string enregistrer_infos(InformationsPersonnelles const &
 infos) 
{ 
  std::string nom_fichier { infos.prenom + "." + infos.nom + ".csv" }; 
  std::ofstream fichier { nom_fichier }; 
 
  fichier << infos.nom << ',' << infos.prenom << ',' << infos.sexe << ',' << infos.age; 
  return nom_fichier; 
} 
 
int main() 
{ 
  auto infos = demander_infos(); 
  auto nom_fichier = enregistrer_infos(infos); 
  std::cout << std::endl << "Ces données seront enregistrées dans le fichier " << nom_fichier << std::endl; 
 
  return 0; 
}