# On n'autorise l'utilisation qu'à
# partir de la version 3.14.0.
cmake_minimum_required (VERSION 3.14.0)

# Il s'agit du projet Discographie.
project (Discographie)

# Les fichiers d'en-tête.
set (HEADERS
  discographie.hpp
  donnees_disco.hpp
  donnees_disco_tests.hpp
  systeme_commandes.hpp
  utils.hpp
)

# Les fichiers sources.
set (SOURCES
  discographie.cpp
  donnees_disco.cpp
  donnees_disco_tests.cpp
  systeme_commandes.cpp
  utils.cpp
  main.cpp
)

# On veut un exécutable Discographie compilé à partir
# des fichiers décrits par SOURCES et HEADERS.
# Ce nom doit être unique, car CMake s'en sert ensuite
# pour définir des cibles, pour lesquelles on pourra
# préciser la norme C++ à utiliser, les éventuelles
# bibliothèques externes, etc.

add_executable (Discographie ${SOURCES} ${HEADERS})
# On veut compiler avec C++20. Si ce n'est pas possible,
# le compilateur rebasculera sur une norme plus 
# ancienne et réessayera. Dans les cas où l'on veut
# forcer l'utilisation de la norme C++20, on peut
# définir en plus la propriété CXX_STANDARD_REQUIRED
# à ON.
set_target_properties(Discographie PROPERTIES
  CXX_STANDARD 20
)