#include "donnees_disco_tests.hpp" 
#include "donnees_disco.hpp"

#include <cassert> 
#include <sstream> 

// Test qu'une entrée complète est valide. 
void test_creation_morceau_entree_complete() 
{ 
  std::istringstream entree { "Chanson A | Mes supers chansons | Bob le chanteur" }; 
  Morceau morceau {}; 
 
  entree >> morceau; 
 
  assert(morceau.nom == "Chanson A" && u8"Le nom du morceau doit être Chanson A."); 
  assert(morceau.album.nom == "Mes supers chansons" && u8"Le nom de l'album doit être Mes supers chansons."); 
  assert(morceau.compositeur.nom == "Bob le chanteur" && u8"Le nom du compositeur doit être Bob le chanteur."); 
} 
 
// Test d'une entrée complète avec beaucoup d'espaces. 
void test_creation_morceau_entree_espaces_partout() 
{ 
  std::istringstream entree { "Chanson B       |  Mes supers chansons   |     Bob le chanteur" };
  Morceau morceau {}; 
 
  entree >> morceau; 
 
  assert(morceau.nom == "Chanson B" && u8"Le nom du morceau doit être Chanson B.");
  assert(morceau.album.nom == "Mes supers chansons" && u8"Le nom de l'album doit être Mes supers chansons.");
  assert(morceau.compositeur.nom == "Bob le chanteur" && u8"Le nom du compositeur doit être Bob le chanteur.");
}

// Test d'une entrée avec seulement le nom de la chanson et de l'artiste. 
void test_creation_morceau_entree_chanson_artiste() 
{ 
  std::istringstream entree { "Un chat qui dormait | | Alice la chanteuse" }; 
  Morceau morceau {}; 
 
  entree >> morceau; 
 
  assert(morceau.nom == "Un chat qui dormait" && u8"Le nom du morceau doit être Un chat qui dormait."); 
  assert(morceau.album.nom == "Album inconnu" && u8"Le nom de l'album doit être Album inconnu."); 
  assert(morceau.compositeur.nom == "Alice la chanteuse" && u8"Le nom du compositeur doit être Alice la chanteuse."); 
} 
 
// Test d'une entrée avec seulement le nom de la chanson. 
void test_creation_morceau_entree_chanson_uniquement() 
{ 
  std::istringstream entree { "L'amour | |" }; 
  Morceau morceau {}; 
 
  entree >> morceau; 
 
  assert(morceau.nom == "L'amour" && u8"Le nom du morceau doit être L'amour."); 
  assert(morceau.album.nom == "Album inconnu" && u8"Le nom de l'album doit être Album inconnu."); 
  assert(morceau.compositeur.nom == "Artiste inconnu" && u8"Le nom du compositeur doit être Artiste inconnu."); 
} 
 
// Test d'une entrée vide. 
void test_creation_morceau_entree_vide() 
{ 
  std::istringstream entree{ "| |" }; 
  Morceau morceau{}; 
 
  entree >> morceau; 
 
  assert(morceau.nom == "Morceau inconnu" && u8"Le nom du morceau doit être Morceau inconnu."); 
  assert(morceau.album.nom == "Album inconnu" && u8"Le nom de l'album doit être Album inconnu."); 
  assert(morceau.compositeur.nom == "Artiste inconnu" && u8"Le nom du compositeur doit être Artiste inconnu."); 
} 