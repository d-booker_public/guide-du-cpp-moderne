#include "discographie.hpp" 
#include "systeme_commandes.hpp" 
#include "utils.hpp" 

#include <sstream> 
#include <stdexcept> 
#include <string> 
 
std::string recuperer_commande() 
{ 
  std::cout << "> "; 
 
  std::string commande {}; 
  std::getline(std::cin, commande); 
  return commande; 
}

// L'ensemble des actions qu'il est possible de faire. 
enum class Commande { Afficher, Ajouter, Enregistrer, Charger, Quitter }; 
 
// Analyse du texte reçu en entrée. 
std::tuple<Commande, std::string> analyser_commande(std::string const & commande_texte)
{
  // Pour traiter la chaîne comme étant un flux, afin d'en extraire les données. 
  std::istringstream flux { commande_texte }; 
 
  std::string premier_mot {}; 
  flux >> premier_mot; 
  premier_mot = traitement_chaine(premier_mot); 
 
  std::string instructions {}; 
  std::getline(flux, instructions); 
  instructions = traitement_chaine(instructions); 
 
  if (premier_mot == "afficher") 
  { 
    // Le mode d'affichage. 
    return { Commande::Afficher, instructions }; 
  } 
  else if (premier_mot == "ajouter") 
  { 
    // Les informations à ajouter. 
    return { Commande::Ajouter, instructions }; 
  } 
  else if (premier_mot == "enregistrer") 
  { 
    // Le fichier à écrire. 
    return{ Commande::Enregistrer, instructions }; 
  } 
  else if (premier_mot == "charger") 
  { 
    // Le fichier à lire. 
    return { Commande::Charger, instructions }; 
  } 
  else if (premier_mot == "quitter") 
  { 
    // Chaîne vide, car on quitte le programme sans autre instruction. 
    return { Commande::Quitter, std::string {} }; 
  }
  else 
  { 
    // On a reçu du texte qui est incorrect. 
    throw std::runtime_error("Commande invalide."); 
  }
}

// La commande à exécuter ainsi que la suite des instructions. 
bool executer_commande(Discographie & discographie, Commande commande, std::string const & instructions) 
{ 
  if (commande == Commande::Afficher) 
  { 
    if (instructions == "artistes") 
    { 
      affichage(discographie, Affichage::Artiste); 
    } 
    else if (instructions == "albums") 
    { 
      affichage(discographie, Affichage::Album); 
    } 
    else if (instructions == "morceaux") 
    { 
      affichage(discographie, Affichage::Morceau); 
    } 
    else 
    { 
      // Si on se trouve ici, alors c'est qu'il y a
      // une erreur dans les instructions d'affichage. 
      throw std::runtime_error("Commande d'affichage inconnue."); 
    } 
  } 
  else if (commande == Commande::Ajouter) 
  { 
    std::istringstream flux { instructions }; 
    Morceau morceau {}; 
    flux >> morceau; 
    discographie.push_back(morceau); 
  } 
  else if (commande == Commande::Charger) 
  { 
    chargement(discographie, instructions); 
  } 
  else if (commande == Commande::Enregistrer) 
  { 
    enregistrement(discographie, instructions); 
  } 
  else if (commande == Commande::Quitter) 
  { 
    // Plus rien à faire, on quitte. 
    return false; 
  } 
 
  return true; 
}