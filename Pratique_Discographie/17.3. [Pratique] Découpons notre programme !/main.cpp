#include "donnees_disco_tests.hpp"
#include "discographie.hpp"
#include "systeme_commandes.hpp"
#include "utils.hpp"
#include <stdexcept>

int main()
{
  // Lancement préalable des tests unitaires. 
  test_creation_morceau_entree_complete();
  test_creation_morceau_entree_espaces_partout(); 
  test_creation_morceau_entree_chanson_artiste(); 
  test_creation_morceau_entree_chanson_uniquement(); 
  test_creation_morceau_entree_vide(); 
 
  // On préremplit la discographie.
  Artiste const bob_le_chanteur { "Bob le chanteur " };
  Artiste const alice_la_chanteuse { "Alice la chanteuse" };
  Artiste const papa_cuistot { "Papa cuistot" };

  Album const mes_supers_chansons { "Mes supers chansons" };
  Album const comptines_alice { "Comptines d'Alice" };
  Album const cuisine_en_musique { "Cuisine en musique" };

  Morceau const chanson_a { "Chanson A", bob_le_chanteur, mes_supers_chansons };
  Morceau const chanson_b { "Chanson B", bob_le_chanteur, mes_supers_chansons };
  Morceau const un_chat_qui_dormait { "Un chat qui dormait", alice_la_chanteuse, comptines_alice };
  Morceau const amour { "L'amour", alice_la_chanteuse, comptines_alice };
  Morceau const mousse_au_chocolat { "La mousse au chocolat", papa_cuistot, cuisine_en_musique };

  Discographie discographie { chanson_a, chanson_b, un_chat_qui_dormait, amour, mousse_au_chocolat };

  bool continuer { true };
  do
  {
    try
    {
      std::string entree { recuperer_commande() }; 
      auto[commande, instructions] = analyser_commande(entree); 
      instructions = traitement_chaine(instructions);
      continuer = executer_commande(discographie, commande, instructions); 
    }
    catch (std::runtime_error const & exception) 
    { 
      std::cout << "Erreur : " << exception.what() << std::endl; 
    } 
 
  } while (continuer);

  return 0;
}