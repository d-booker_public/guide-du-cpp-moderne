// On a besoin de iostream pour utiliser std::cout,
// std::quoted et std::endl. 
#include <iostream> 
// On inclut le fichier d'en-tête correspondant. 
#include "test.hpp" 
 
// Par contre, comme 'test.hpp' inclut déjà '<string>',
// on n'a pas besoin de l'inclure de nouveau ici,
// dans le fichier source. 
 
// On définit la fonction ici. 
void afficher_message(std::string const & message) 
{ 
  std::cout << "J'ai reçu un message : " << std::quoted(message) << std::endl; 
}