// Parce que le compilateur a besoin de savoir ce qu'est std::string. 
#include <string> 
 
// On ne met ici que le prototype. 
void afficher_message(std::string const & message);