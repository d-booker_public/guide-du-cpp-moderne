// Pour appeler la fonction, il faut connaître son prototype,
// donc on doit inclure le fichier d'en-tête correspondant. 
#include "test.hpp" 
 
int main() 
{ 
  // Ici, on va utiliser la fonction. 
  afficher_message("Salut les lecteurs !"); 
  return 0; 
} 