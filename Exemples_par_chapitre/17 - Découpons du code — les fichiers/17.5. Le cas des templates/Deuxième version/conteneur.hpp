#ifndef CONTENEUR_HPP 
#define CONTENEUR_HPP 
 
#include <iostream> 
 
template <typename Collection> 
void afficher(Collection const & iterable) 
{ 
  for (auto const & e : iterable) 
  { 
    std::cout << e << std::endl; 
  } 
} 
 
#endif