#ifndef CONTENEUR_HPP 
#define CONTENEUR_HPP 
 
#include <iostream> 
 
template <typename Collection> 
void afficher(Collection const & iterable); 
 
// A la fin, on rajoute le fichier d'implémentation. 
#include "conteneur_impl.tpp" 
 
#endif 