#include <iostream> 
#include <vector> 

// Le fichier d'implémentation est lui aussi inclus, du coup. 
#include "conteneur.hpp" 
 
int main() 
{ 
  std::vector<int> const tableau { 1, 2, 3, 4 }; 
  afficher(tableau); 
  return 0; 
} 