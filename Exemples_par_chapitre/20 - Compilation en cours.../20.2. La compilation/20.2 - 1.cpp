template <typename T>
void fonction(T const & t)
{
  T const resultat { t * 2 };
}

int main()
{
  fonction(42);
  fonction(3.1415);
  return 0;
}