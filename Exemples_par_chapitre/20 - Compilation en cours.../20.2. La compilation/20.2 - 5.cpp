#include <iostream>

double fonction()
{
  return 3.141582;
}

int main()
{
  int const x { fonction() };
  std::cout << "Voici un nombre : " << x << std::endl;
  return 0;
}