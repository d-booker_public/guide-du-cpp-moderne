#include <array>
#include <iostream>

constexpr int factorielle(int n)
{
  int resultat { 1 };
  for (int i { 1 }; i <= n; ++i)
  {
    resultat *= i;
  }
  return resultat;
}

int main()
{
  // Exécuté à la compilation.
  std::array<int, factorielle(3)> tableau {};

  // Exécuté quand le programme est lancé.
  int entier { 0 };
  std::cout << "Donne un entier entre 1 et 10 : ";
  std::cin >> entier;
  std::cout << "Sa factorielle vaut " << factorielle(entier) << "." << std::endl;

  return 0;
}