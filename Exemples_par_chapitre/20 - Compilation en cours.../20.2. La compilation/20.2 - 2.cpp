template <typename T = int> void fonction(const int &t)
{
  const int resultat{t * 2};
}

template <typename T = double> void fonction(const double &t)
{
  const double resultat{t * 2};
}

template <typename T> void fonction(const T &t)
{
  const T resultat{t * 2};
}

int main()
{
  fonction(42);
  fonction(3.1415);
  return 0;
}