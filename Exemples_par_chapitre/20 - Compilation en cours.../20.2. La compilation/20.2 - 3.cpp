int main()
{
  // Valide parce que 5 est un littéral évaluable à la compilation.
  constexpr int const x { 5 };
  // Valide parce que x est constexpr.
  constexpr int const y { 2 * x };
  
  // Invalide car b n'est pas une constante.
  int b { 10 };
  constexpr int const non { b - 5 };
  return 0;
}