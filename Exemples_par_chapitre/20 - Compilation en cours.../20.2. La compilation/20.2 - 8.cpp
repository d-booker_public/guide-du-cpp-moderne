#include <iostream>
#include <string>

void fonction(std::string const & chaine, bool en_majuscule)
{
  std::cout << chaine << std::endl;
}

int main()
{
  std::string const une_phrase { "Du texte." };
  fonction(une_phrase, true);
  return 0;
}