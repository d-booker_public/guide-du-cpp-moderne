#include <iostream>

double fonction()
{
  return 3.141582;
}

int main()
{
  int const x { static_cast<int>(fonction()) };
  std::cout << "Voici un nombre : " << x << std::endl;
  
  return 0;
}