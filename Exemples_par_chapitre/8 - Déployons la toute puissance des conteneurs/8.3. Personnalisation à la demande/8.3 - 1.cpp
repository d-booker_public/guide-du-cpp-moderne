#include <functional>
#include <iostream>

int main()
{
  std::cout << std::boolalpha;

  // On peut l'utiliser directement.
  std::cout << std::greater<int>{}(2, 3) << std::endl;
  std::cout << std::greater<int>{}(6, 3) << std::endl;
  std::cout << std::greater<double>{}(0.08, 0.02) << std::endl;

  // Ou bien à travers une variable, qu'on appelle comme une fonction.
  std::greater<int> foncteur {};
  std::cout << foncteur(-78, 9) << std::endl;
  std::cout << foncteur(78, 8) << std::endl;

  return 0;
}