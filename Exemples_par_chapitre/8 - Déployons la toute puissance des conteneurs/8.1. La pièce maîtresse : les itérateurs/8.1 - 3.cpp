#include <iostream>
#include <vector>

int main()
{
  std::vector<int> tableau { -1, 28, 346 };
  auto debut_tableau { std::begin(tableau) };

  std::cout << *debut_tableau << std::endl;
  // On incrémente l'itérateur pour pointer sur l'élément suivant.
  ++debut_tableau;
  // On affiche le deuxième élément.
  std::cout << *debut_tableau << std::endl;

  return 0;
}