#include <iostream>
#include <vector>

int main()
{
  std::vector<int> const tableau { -1, 28, 346, 84 };
  
  // Affiche depuis la fin, en commençant par 84.
  for (auto it { std::rbegin(tableau) }; it != std::rend(tableau); ++it)
  {
    std::cout << *it << std::endl;
  }

  return 0;
}