#include <iostream>
#include <vector>

int main()
{
  std::vector<int> tableau { -1, 28, 346, 84 };
  
  for (auto it { std::begin(tableau) }; it != std::end(tableau); ++it)
  {
    std::cout << *it << std::endl;
  }

  return 0;
}