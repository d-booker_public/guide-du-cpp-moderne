#include <vector>

int main()
{
  std::vector<int> const tableau {-1, 28, 346};

  // Déclaration explicite d'un itérateur de std::vector d'entiers.
  std::vector<int>::const_iterator debut_tableau {std::begin(tableau)};
  // Déclaration plus courte en utilisant auto.
  auto fin_tableau {std::end(tableau)};
  
  return 0;
}