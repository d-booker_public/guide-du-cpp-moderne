#include <string>

int main()
{
  std::string mot { "Bonjour" };

  // Itérateur constant sur un élément modifiable.
  auto const iterateur_constant { std::begin(mot) };

  // Ceci est impossible.
  // it = std::end(mot);

  // Ceci est parfaitement autorisé.
  *iterateur_constant = 'B';

  auto iterateur_non_constant { std::cbegin(mot) };

  // Ceci est impossible.
  // *iterateur_non_constant = 'D';

  // Ceci est parfaitement autorisé.
  iterateur_non_constant = std::cend(mot) - 1;
  
  return 0;
}