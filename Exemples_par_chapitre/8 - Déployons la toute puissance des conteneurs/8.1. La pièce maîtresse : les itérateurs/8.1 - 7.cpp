#include <string>
#include <iostream>

int main()
{
  std::string mot { "Bonjour" };

  // Itérateur constant sur un élément modifiable.
  auto const it { std::begin(mot) };

  // Ceci est impossible.
  // it = std::end(mot);

  // Ceci est parfaitement autorisé.
  *it = 'B';

  std::cout << mot << std::endl;
  return 0;
}