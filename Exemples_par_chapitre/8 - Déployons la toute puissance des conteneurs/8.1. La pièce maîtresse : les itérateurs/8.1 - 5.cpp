#include <iostream>
#include <vector>

int main()
{
  std::vector<int> tableau { -1, 28, 346, 84 };
  auto iterateur_dernier_element { std::end(tableau) };

  // On déplace l'itérateur sur l'élément précédent.
  --iterateur_dernier_element;

  // Et on affiche pour vérifier.
  std::cout << *iterateur_dernier_element << std::endl;
  return 0;
}