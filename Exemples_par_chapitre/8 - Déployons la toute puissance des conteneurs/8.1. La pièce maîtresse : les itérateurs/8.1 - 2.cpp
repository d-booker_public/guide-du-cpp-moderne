#include <iostream>
#include <string>

int main()
{
  std::string tableau {"Tu texte."};

  auto debut_tableau {std::begin(tableau)};

  // On affiche le premier élément du tableau.
  std::cout << *debut_tableau << std::endl;

  // Et on peut même le modifier, comme ce qu'on fait
  // avec les références, ou les crochets.
  *debut_tableau = 'D';
  
  // On vérifie.
  std::cout << tableau << std::endl;
  return 0;
}