#include <iostream>
#include <vector>

int main()
{
  std::vector<int> tableau { -1, 28, 346, 84, -94, 31, 784 };

  // Notez le +4 pour accéder au cinquième élément,
  // car comme avec les crochets, on commence à 0.
  auto iterateur_cinquieme_element { std::begin(tableau) + 4 };
  std::cout << *iterateur_cinquieme_element << std::endl;

  return 0;
}