#include <algorithm>
#include <iostream>
#include <vector>

int main()
{
  std::vector<int> const premier { 1, 2, 3, 4 };
  std::vector<int> const second { 1, 2, 3, 4, 5 };
  std::vector<int> const troisieme { 1, 2, 3, 4 };
  
  std::cout << std::boolalpha;
  std::cout << std::equal(std::begin(premier), std::end(premier), std::begin(second), std::end(second)) << std::endl;
  std::cout << std::equal(std::begin(premier), std::end(premier), std::begin(troisieme), std::end(troisieme)) << std::endl;
}