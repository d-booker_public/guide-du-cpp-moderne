#include <algorithm>
#include <iostream>
#include <string>

int main()
{
  std::string const phrase { "Exemple illustrant le Guide du C++ moderne." };

  // Toute la collection.
  auto const total_phrase { std::count(std::begin(phrase), std::end(phrase), 'e') };
  std::cout << "Dans la phrase entière, il y a " << total_phrase << " 'e' minuscule.\n";
  
  // Un sous-ensemble seulement de la collection.
  auto const total_premier_mot { std::count(std::begin(phrase), std::begin(phrase) + 7, 'e') };
  std::cout << "Dans le premier mot, il y a " << total_premier_mot << " 'e' minuscule.\n";

  return 0;
}