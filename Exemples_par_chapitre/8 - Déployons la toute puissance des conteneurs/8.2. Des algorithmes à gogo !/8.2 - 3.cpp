#include <algorithm>
#include <iostream>
#include <vector>

int main()
{
  std::vector<double> constantes_mathematiques { 2.71828, 3.1415, 1.0836, 1.4142, 1.6180 };
  std::sort(std::begin(constantes_mathematiques), std::end(constantes_mathematiques));

  for (auto constante : constantes_mathematiques)
  {
    std::cout << constante << std::endl;
  }
  
  return 0;
}