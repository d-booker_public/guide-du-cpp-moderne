#include <algorithm>
#include <iostream>
#include <list>

int main()
{
  std::list<int> nombres { 2, 3, 1, 7, -4 };
  
  std::cout << "Avant :\n";
  for (auto nombre : nombres)
  {
    std::cout << nombre << std::endl;
  }

  std::reverse(std::begin(nombres), std::end(nombres));

  std::cout << "\nAprès :\n";
  for (auto nombre : nombres)
  {
    std::cout << nombre << std::endl;
  }
  
  return 0;
}