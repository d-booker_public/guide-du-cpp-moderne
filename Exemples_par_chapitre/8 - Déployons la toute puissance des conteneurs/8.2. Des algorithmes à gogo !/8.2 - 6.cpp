#include <algorithm>
#include <iostream>
#include <string>

int main()
{
  std::string paroles { "I'm blue, da ba dee da ba daa !" };

  auto iterateur_fin { std::remove(std::begin(paroles), std::end(paroles), 'b')};
  paroles.erase(iterateur_fin, std::end(paroles));
  std::cout << paroles << std::endl;

  return 0;
}