#include <algorithm>
#include <iostream>
#include <string>

int main()
{
  std::string const phrase { "Un exemple de phrase avec plusieurs mots." };

  auto const debut { std::begin(phrase) };
  auto const fin { std::end(phrase) };

  // Rappel : std::boolalpha permet d'afficher
  // 'true' ou 'false' et non '1' ou '0'.
  std::cout << std::boolalpha;

  std::string mot { "mot" };
  std::cout << (std::search(debut, fin, std::begin(mot), std::end(mot)) != fin) << std::endl;
  
  mot = "exemple";
  std::cout << (std::search(debut, fin, std::begin(mot), std::end(mot)) != fin) << std::endl;

  return 0;
}