#include <iostream>

int main()
{
  int un_entier { 42 };
  
  // Le mot clé 'mutable' se place après les paramètres
  // de la lambda et avant le type de retour.
  auto lambda = [un_entier]() mutable -> void
  {
    un_entier = 0;
    std::cout << "L'entier vaut " << un_entier << " dans la lambda.\n"
  };

  lambda();

  std::cout << "L'entier vaut " << un_entier << " dans main.\n";
  return 0;
}