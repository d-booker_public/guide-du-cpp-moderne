#include <iostream>

int main()
{
  bool const un_booleen { true };
  int const un_entier { 42 };
  double const un_reel { 3.1415 };

  auto lambda = [un_booleen, un_entier, un_reel]() -> void
  {
    std::cout << "Le booléen vaut " << std::boolalpha << un_booleen << "." << std::endl;
    std::cout << "L'entier vaut " << un_entier << "." << std::endl;
    std::cout << "Le réel vaut " << un_reel << "." << std::endl;
  };
  
  lambda();
  return 0;
}