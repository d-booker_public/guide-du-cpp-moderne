#include <algorithm>
#include <iostream>
#include <vector>

int main()
{
  std::vector<int> const nombres { 8, 7, 5, 4, 31, 98, 2, 77, 648 };
  
  int const diviseur { 2 };
  int somme { 0 };

  // Il n'y a aucun problème à mélanger les captures par valeur et par référence.
  std::for_each(std::cbegin(nombres), std::cend(nombres), [diviseur, &somme](int element) -> void
  {
    if (element % diviseur == 0)
    {
      somme += element;
    }
  });

  std::cout << "La somme de tous les éléments divisibles par " << diviseur << " vaut " << somme << "." << std::endl;
  return 0;
}