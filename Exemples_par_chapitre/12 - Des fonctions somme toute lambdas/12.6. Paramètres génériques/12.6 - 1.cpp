#include <iostream>
#include <string>

int main()
{
  // Le premier paramètre est générique, mais le deuxième
  // est explicitement marqué comme étant une chaîne.
  auto lambda = [](auto const & parametre, std::string const & message) -> void {
    std::cout << "Paramètre générique reçu : " << parametre << std::endl;
    std::cout << "Message reçu : " << message << std::endl;
  };
  
  lambda(1, "Test avec un entier.");
  lambda("str", "Test avec une chaîne de caractères.");
  lambda(3.1415, "Test avec un flottant.");

  return 0;
}