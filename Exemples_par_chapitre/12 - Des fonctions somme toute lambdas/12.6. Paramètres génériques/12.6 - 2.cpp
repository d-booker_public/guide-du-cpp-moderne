#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

int main()
{
  auto enumeration = [](auto const & parametre) -> void
  {
    std::cout << "Paramètre reçu : " << parametre << std::endl;
  };
  
  // On affiche une collection de std::string.
  std::vector<std::string> const chaines {
    "Un mot",
    "Autre chose",
    "Du blabla",
    "Du texe",
    "Des lettres"
  };
  std::for_each(std::begin(chaines), std::end(chaines), enumeration);
  
  std::cout << std::endl;
  
  // Pas de problème non plus avec des entiers.
  std::vector<int> const nombres { 1, 5, -7, 67, -4454, 7888 };
  std::for_each(std::begin(nombres), std::end(nombres), enumeration);
  
  return 0;
}