#include <iostream>
#include <vector>

class Matrice
{
public:
  Matrice() = default;
  // Constructeur de copie.
  Matrice(Matrice const& matrice);
  // Constructeur de déplacement.
  Matrice(Matrice&& matrice) noexcept;
  Matrice& operator+=(Matrice const& autre);
};

Matrice::Matrice(Matrice const& matrice)
{
  std::cout << "Je suis dans le constructeur de copie de la classe Matrice.\n";
}

Matrice::Matrice(Matrice&& matrice) noexcept
{
  std::cout << "Je suis dans le constructeur de déplacement de la classe Matrice.\n";
}

Matrice& Matrice::operator+=(Matrice const& autre)
{
  std::cout << "3\n";
  std::cout << "Je suis dans l'opérateur += de la classe Matrice.\n";
  return *this;
}

Matrice operator+(Matrice lhs, Matrice const& rhs)
{
  std::cout << "2\n";
  lhs += rhs;
  std::cout << "4\n";
  return lhs;
}

int main()
{
  Matrice const m1 {};
  Matrice const m2 {};

  std::vector<Matrice> matrices{};
  std::cout << "1\n";
  matrices.push_back(m1 + m2);

  return 0;
}