#include <iostream>
#include <string>
#include <utility>
#include <vector>

struct Test
{
  Test(std::vector<int> const & tableau, int entier);
  ~Test() noexcept = default;

  // On demande la génération explicite du déplacement.
  Test(Test&& deplacement) noexcept = default;
  Test& operator=(Test&& autre_test) noexcept = default;

  friend std::ostream& operator<<(std::ostream& os, Test const& test);

private:
  std::vector<int> m_tableau;
  int m_entier;
};

Test::Test(std::vector<int> const& tableau, int entier)
  : m_tableau(tableau), m_entier(entier)
{
}

std::ostream& operator<<(std::ostream& os, Test const& test)
{
  return os << "adresse m_tableau -> " << &test.m_tableau
    << " | adresse ressource gérée -> " << test.m_tableau.data()
    << " | adresse m_entier -> " << &test.m_entier;
}

int main()
{
  std::vector<int> const entiers { 1, 2, 3, 4, 5 };

  Test t1 { entiers, 42 };
  std::cout << "t1 : " << t1 << std::endl;
  std::vector<int> const autres_entiers { 0 };

  Test t2 { autres_entiers, -7 };
  std::cout << "t2 : " << t2 << std::endl;

  std::cout << "\nOn déplace.\n";
  t2 = std::move(t1);
  std::cout << "t2 : " << t2 << std::endl;

  return 0;
}