#include <iostream>
#include <string>

std::string source() noexcept
{
  return "src";
}

void f(std::string && s) noexcept
{
  std::cout << "f(&&)\n";
}

int main()
{
  std::string varloc { "vl" };
  std::cout << "f(varloc) -> ";
  // On utilise std::move, ce qui provoque un appel à f(&&).
  f(std::move(varloc));

  std::cout << "f(source()) -> ";
  // Source est déjà une rvalue, le cast est donc inutile.
  f(source());

  // Mais du coup, rien n'a changé ?
  std::cout << "varloc -> " << varloc << std::endl;
  std::string un_puit { std::move(varloc) };

  // Pourtant ici, on voit bien que le contenu de varloc a été déplacé, qu'il n'y a plus rien.
  std::cout << "un_puit -> " << un_puit << std::endl;
  std::cout << "varloc -> " << varloc << std::endl;

  return 0;
}