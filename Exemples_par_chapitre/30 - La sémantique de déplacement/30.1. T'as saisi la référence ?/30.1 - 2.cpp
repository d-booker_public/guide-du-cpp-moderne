#include <memory>
#include <vector>

class Employe
{
};

class Entreprise
{
public:
  void ajouter_employe_ref(std::unique_ptr<Employe> && employe);

private:
  std::vector<std::unique_ptr<Employe>> m_employes;
};

void Entreprise::ajouter_employe_ref(std::unique_ptr<Employe> && employe)
{
  // std::move permet le transfert de responsabilité entre le paramètre et le std::vector.
  m_employes.push_back(std::move(employe));
}

int main()
{
  Entreprise entreprise {};
  entreprise.ajouter_employe_ref(std::make_unique<Employe>());

  return 0;
}