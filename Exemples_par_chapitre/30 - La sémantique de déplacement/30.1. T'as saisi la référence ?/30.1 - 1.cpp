#include <memory>
#include <vector>

class Employe
{
public:
  Employe(Employe const& origine) = delete;
  Employe& operator=(Employe const& autre_employe) = delete;
  virtual ~Employe() noexcept = default;
  // Autres services...
};

class Entreprise
{
public:
  void embaucher_employe_const_ref(std::unique_ptr<Employe> const& employe);
  void embaucher_employe_ref(std::unique_ptr<Employe>& employe);

private:
  std::vector<std::unique_ptr<Employe>> m_employes;
};

void Entreprise::embaucher_employe_const_ref(std::unique_ptr<Employe> const& employe)
{
  m_employes.push_back(employe);
}

void Entreprise::embaucher_employe_ref(std::unique_ptr<Employe>& employe)
{
  m_employes.push_back(employe);
}

int main()
{
  Entreprise entreprise {};

  // Les deux lignes suivantes posent problème.
  entreprise.embaucher_employe_const_ref(std::make_unique<Employe>());
  entreprise.embaucher_employe_ref(std::make_unique<Employe>());

  return 0;
}