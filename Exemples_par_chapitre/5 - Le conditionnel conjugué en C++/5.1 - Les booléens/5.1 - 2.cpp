#include <iostream>

int main()
{
  int const a { 10 };
  int const b { 20 };

  // Cette directive nous permet d'afficher true ou false.
  // Par défaut, std::cout affiche 1 ou 0.
  std::cout << std::boolalpha;
  std::cout << "a == b donne " << (a == b) << std::endl;
  std::cout << "a != b donne " << (a != b) << std::endl;
  std::cout << "a < b donne " << (a < b) << std::endl;
  std::cout << "a <= b donne " << (a <= b) << std::endl;
  
  // On peut tout à fait stocker le résultat
  // dans une variable booléenne.
  bool const plus_grand { a > b };
  std::cout << "a > b donne " << plus_grand << std::endl;
  bool const plus_grand_ou_egal { a >= b };
  std::cout << "a >= b donne " << plus_grand_ou_egal << std::endl;
 
  return 0;
}