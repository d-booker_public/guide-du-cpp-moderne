#include <iostream>

int main()
{
  std::cout << "Bienvenue au restaurant \"Chez Clem\"." << std::endl;

  bool const plat_vegetarien {true};
  if (plat_vegetarien)
  {
    bool const salade {true};
    if (salade)
    {
      std::cout << "Salade grecque ? De pâtes ?" << std::endl;
    }
    else
    {
      std::cout << "Nous avons également de la purée, une macédoine de légumes, une omelette... " << std::endl;
    }
  }
  else
  {
    bool const viande {true};
    if (viande)
    {
      std::cout << "Bleue, saignante, à point, bien cuite ?" << std::endl;
    }
    else
    {
      std::cout << "Sinon niveau poisson il y a pavé de saumon, filet de cabillaud... " << std::endl;
    }
  }

  return 0;
}