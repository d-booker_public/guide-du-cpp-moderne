#include <iostream>

int main()
{
  bool const ai_je_envie_de_nager {true};
  bool const ai_je_un_maillot {true};
  int const argent {20};
  int const prix {5};
  
  if (ai_je_envie_de_nager && ai_je_un_maillot && argent >= prix)
  {
    std::cout << "PLOUF !" << std::endl;
  }
  else
  {
    std::cout << "Huum, un autre jour." << std::endl;
  }

  return 0;
}