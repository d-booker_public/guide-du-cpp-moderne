#include <iostream>
int main()
{
  bool const ai_je_une_voiture {false};
  bool const habite_immeuble {true};

  // Comme ai_je_une_voiture vaut false, le compilateur
  // ne va pas tester l'expression habite_immeuble.
  if (ai_je_une_voiture && habite_immeuble)
  {
    std::cout << "Voici votre place de parking.\n";
  }
  else
  {
    std::cout << "Désolé, vous n'êtes pas éligible.\n";
  }

  return 0;
}
