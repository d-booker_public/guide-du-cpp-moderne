#include <iostream>

int main()
{
  if (true)
  {
    int const age {42};
    // D'autres instructions.
    // Aucun problème.
    if (age == 42)
    {
      // Ici non plus.
    }
  }
  
  // NON ! Le compilateur ne connait pas la variable age.
  std::cout << age << std::endl;
  return 0;
}