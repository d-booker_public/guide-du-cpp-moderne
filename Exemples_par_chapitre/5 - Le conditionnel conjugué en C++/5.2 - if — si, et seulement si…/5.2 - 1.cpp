#include <iostream>

int main()
{
  std::cout << "Indique-moi ta note : ";
  int note { 0 };
  std::cin >> note;
  std::cout << "Tu as obtenu " << note << std::endl;
 
  if (note >= 16)
  {
    std::cout << "Félicitations, c'est une très bonne note !\n";
  }
  return 0;
}
