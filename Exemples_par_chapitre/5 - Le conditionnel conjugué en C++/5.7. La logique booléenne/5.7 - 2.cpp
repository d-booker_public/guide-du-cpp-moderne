#include <iostream>

int main()
{
  int const age {25};
  bool const est_professeur {true};

  if (age <= 26 || est_professeur)
  {
    std::cout << "Bonne visite.\n";
  }
  else
  {
    std::cout << "Désolé, il va falloir payer.\n";
  }

  return 0;
}