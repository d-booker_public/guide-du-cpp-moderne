int main()
{
  bool const ai_je_un_maillot {true};
  if (ai_je_un_maillot)
  {
    // Je n'ai rien à faire, tout est OK.
  }
  else
  {
    // Je dois aller acheter un maillot.
  }
  // Je nage et je suis heureux.
  return 0;
}