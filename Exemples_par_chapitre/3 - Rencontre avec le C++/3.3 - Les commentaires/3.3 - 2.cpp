#include <iostream>

int main()
{
  /* Ceci est un commentaire sur une seule ligne. */
  std::cout << "Hello World !" << std::endl; /* Ceci est un commentaire à la fin d'une ligne du code. */
  
  /* Ceci...
  ...est un commentaire
  écrit sur plusieurs lignes...
  ...car il est très long. */
  std::/* Ceci est un commentaire noyé dans le code. */cout << "Hello World !"; 

  return 0;
} 