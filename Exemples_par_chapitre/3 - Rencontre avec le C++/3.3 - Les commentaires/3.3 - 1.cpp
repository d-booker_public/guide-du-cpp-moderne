#include <iostream>

int main()
{
  // Cette ligne affiche Bonjour.
  std::cout << "Bonjour";

  // Cette ligne permet d'afficher un retour à la ligne.
  std::cout << std::endl;

  return 0;
} 