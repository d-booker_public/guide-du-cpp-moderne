#include <fstream>

int main()
{
  // On veut ouvrir un fichier nommé 'sortie.txt', qui se trouve dans le dossier du projet.
  // S'il existe, il sera ouvert. Sinon, il sera d'abord créé puis ouvert.
  std::ofstream fichier { "sortie.txt" };
  return 0;
}