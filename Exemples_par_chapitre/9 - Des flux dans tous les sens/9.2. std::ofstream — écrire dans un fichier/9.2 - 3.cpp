#include <fstream>
#include <string>

int main()
{
  std::ofstream fichier { "sortie.txt" };
  fichier << 3 << " " << 4;

  int x { 0 };
  // On va à la ligne puis on écrit une équation.
  fichier << '\n' << x << " + 2 = " << x + 2;
  
  // Pas de problème avec le texte non plus.
  std::string texte { "Voici une phrase." };
  fichier << '\n' << texte;
  
  return 0;
}