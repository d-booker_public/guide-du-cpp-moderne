#include <fstream>
#include <iostream>
#include <string>

int main()
{
  std::ofstream fichier { "sortie.txt", std::ios::app };
  
  fichier << 3 << " " << 4;
  
  int x { 0 };
  fichier << '\n' << x << " + 2 = " << x + 2;

  std::string texte { u8"Voici une phrase." };
  fichier << '\n' << texte;
  
  return 0;
}