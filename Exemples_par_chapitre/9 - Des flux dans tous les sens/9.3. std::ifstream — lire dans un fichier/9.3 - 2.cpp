#include <fstream>
#include <iostream>
#include <string>

int main()
{
  std::ifstream fichier { "texte.txt" };
  
  int entier { 0 };
  fichier >> entier;
  std::cout << "Mon entier vaut : " << entier << std::endl;

  std::string mot { "" };
  fichier >> mot;
  std::cout << "Mon mot vaut : " << mot << std::endl;

  return 0;
}