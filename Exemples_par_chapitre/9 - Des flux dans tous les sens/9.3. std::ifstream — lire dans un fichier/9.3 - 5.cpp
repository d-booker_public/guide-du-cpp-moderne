#include <fstream>
#include <iostream>
#include <string>

int main()
{
  std::ifstream fichier { "texte.txt" };
  
  std::string ligne { "" };
  while (std::getline(fichier, ligne))
  {
    std::cout << "Ligne lue : " << ligne << std::endl;
  }

  return 0;
}