#include <fstream>
#include <iostream>
#include <string>

int main()
{
  std::ifstream fichier { "texte.txt" };
  
  int entier { 0 };
  fichier >> entier;
  std::cout << "Mon entier vaut : " << entier << std::endl;

  std::string phrase { "" };
  std::getline(fichier, phrase);
  std::cout << "Ma phrase vaut : " << phrase << std::endl;

  return 0;
}