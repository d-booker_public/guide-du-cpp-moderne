#include <fstream>
#include <iostream>
#include <string>

int main()
{
  std::ofstream fichier { "sortie.txt" };

  // Notez qu'on ne vide pas le tampon.
  fichier << "Hey, salut toi !\n";
  fichier << 42 << " " << 2.718;

  // Une entrée pour vous laisser le temps
  // d'ouvrir le fichier sortie.txt.
  std::string phrase { "" };
  std::cout << "Tape une phrase quelconque : ";
  std::cin >> phrase;

  // On vide explicitement le tampon.
  // Maintenant les données sont écrites.
  fichier << std::flush;

  return 0;
}