#include <fstream>

int main()
{
  std::ofstream fichier { "sortie.txt" };
  // On va écrire true.
  fichier << std::boolalpha << true << std::endl;
  // On revient comme avant et on va écrire 0.
  fichier << std::noboolalpha << false << std::endl;
  
  return 0;
}