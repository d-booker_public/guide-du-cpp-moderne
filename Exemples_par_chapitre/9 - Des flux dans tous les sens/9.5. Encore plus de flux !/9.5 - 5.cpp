#include <iostream>
#include <sstream>

int main()
{
  std::istringstream flux_entree { "f8 ad 32" };

  int rouge { 0 };
  int vert { 0 };
  int bleu { 0 };
  
  flux_entree >> std::hex >> rouge >> vert >> bleu;
  
  std::cout << "Niveau de rouge : " << rouge << std::endl;
  std::cout << "Niveau de vert : " << vert << std::endl;
  std::cout << "Niveau de bleu : " << bleu << std::endl;

  return 0;
}