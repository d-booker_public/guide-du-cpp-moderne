#include <iostream>
#include <sstream>
#include <string>

int main()
{
  double const reel { 10245.789 };

  std::ostringstream flux_chaine;
  // Notez comment l'utilisation est identique à ce que vous connaissez.
  flux_chaine << std::scientific << std::showpos << reel << std::endl;

  // On récupère une chaîne de caractères en appelant str().
  std::string resultat { flux_chaine.str() };
  std::cout << "Affichage par défaut : " << reel << std::endl;
  std::cout << "Affichage modifié : " << resultat << std::endl;

  return 0;
}