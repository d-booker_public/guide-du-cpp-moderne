#include <iostream>

int main()
{
  int jour { 0 };
  std::cout << "Donne un jour entre 1 et 31 : ";
      
  while (!(std::cin >> jour) || jour < 0 || jour > 31)
  {
    std::cout << "Entrée invalide. Recommence." << std::endl;
    std::cin.clear();
    std::cin.ignore(255, '\n');
  }
  
  std::cout << "Jour donné : " << jour << std::endl;
  return 0;
}