#include <iostream>
#include <stdexcept>
#include <string>

int main()
{
  try
  {
    // Essayez avec un nombre très très grand ou bien avec des
    // lettres pour observer le comportement de std::stoi.
    int entier { std::stoi("1000000000000000000000000000000000000000000000000000000") };
    std::cout << "Entier : " << entier << std::endl;
  }
  catch (std::invalid_argument const & exception)
  {
    std::cout << "Argument invalide : " << exception.what() << std::endl;
  }
  catch (std::out_of_range const & exception)
  {
    std::cout << "Chaîne trop longue : " << exception.what() << std::endl;
  }

  return 0;
}