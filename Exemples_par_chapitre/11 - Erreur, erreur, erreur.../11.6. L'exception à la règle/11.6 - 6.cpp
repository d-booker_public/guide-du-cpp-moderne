#include <iostream>
#include <stdexcept>
#include <string>

int main()
{
  try
  {
    int entier { std::stoi("+a") };
    std::cout << "Entier : "<< entier << std::endl;
  }
  catch (std::invalid_argument const & exception)
  {
    // Message particulier affiché seulement dans le cas
    // d'une exception std::invalid_argument.
    std::cout << "Argument invalide : " << exception.what() << std::endl;
  }
  catch (std::exception const & exception)
  {
    // Message générique pour tous les autres types
    // d'exceptions possibles.
    std::cout << "Erreur : " << exception.what() << std::endl;
  }

  return 0;
}