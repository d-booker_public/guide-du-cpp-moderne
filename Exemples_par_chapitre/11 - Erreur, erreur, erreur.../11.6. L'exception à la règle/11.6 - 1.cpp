#include <fstream>
#include <iostream>
#include <string>
#include <vector>

std::vector<std::string> lire_fichier(std::string const & nom_fichier)
{
  std::vector<std::string> lignes {};
  std::string ligne { "" };
  std::ifstream fichier { nom_fichier };

  while (std::getline(fichier, ligne))
  {
    lignes.push_back(ligne);
  }

  return lignes;
}
    
int main()
{
  std::string nom_fichier { "" };

  std::cout << "Donnez un nom de fichier : ";
  std::cin >> nom_fichier;

  auto lignes = lire_fichier(nom_fichier);

  std::cout << "Voici le contenu du fichier :" << std::endl;
  for (auto const & ligne : lignes)
  {
    std::cout << ligne << std::endl;
  }

  return 0;
}