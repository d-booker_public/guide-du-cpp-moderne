#include <iostream>
#include <stdexcept>
#include <string>

int main()
{
  try
  {
    // Essayez avec un nombre très très grand ou bien avec des
    //  lettres pour observer le comportement de std::stoi.
    int entier { std::stoi("1000000000000000000000000000000000000000000000000000000") };
    std::cout << "Entier : " << entier << std::endl;
  }
  catch (std::exception const & exception)
  {
    std::cout << "Une erreur est survenue : " << exception.what() << std::endl;
  }

  return 0;
}