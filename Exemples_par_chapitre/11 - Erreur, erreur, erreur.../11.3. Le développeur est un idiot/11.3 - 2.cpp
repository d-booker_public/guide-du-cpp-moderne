#include <cassert>

int main()
{
  // Va parfaitement fonctionner et passer à la suite.
  assert(1 == 1 && "1 doit toujours être égal à 1.");
  // Va faire planter le programme.
  assert(1 == 2 && "Ouh là, 1 n'est pas égal à 2.");

  return 0;
}