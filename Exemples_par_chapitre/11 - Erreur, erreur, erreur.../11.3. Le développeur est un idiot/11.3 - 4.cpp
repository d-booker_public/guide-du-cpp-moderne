#include <cassert>
#include <iostream>

double division(double numerateur, double denominateur)
{
  assert(denominateur != 0.0 && "Le dénominateur ne peut pas valoir 0.");
  return numerateur / denominateur;
}

int main()
{
  std::cout << "Numérateur : ";
  double numerateur { 0.0 };

  std::cin >> numerateur;
  double denominateur { 0.0 };

  // La responsabilité de vérifier que le contrat est respecté appartient au code appelant.
  do
  {
    std::cout << "Dénominateur : ";
    std::cin >> denominateur;
  } while (denominateur == 0.0);

  std::cout << "Résultat : " << division(numerateur, denominateur) << std::endl;
  return 0;
}