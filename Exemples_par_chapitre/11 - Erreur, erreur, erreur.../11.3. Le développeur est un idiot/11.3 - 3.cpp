#include <cassert>
#include <iostream>
#include <vector>

int main()
{
  std::vector<int> const tableau { -4, 8, 452, -9 };
  int const index { 2 };

  assert(index >= 0 && "L'index ne doit pas être négatif.");
  assert(index < std::size(tableau) && "L'index ne doit pas être plus grand que la taille du tableau.");
  std::cout << "Voici l'élément " << index << " : " << tableau[index] << std::endl;
  
  return 0;
}