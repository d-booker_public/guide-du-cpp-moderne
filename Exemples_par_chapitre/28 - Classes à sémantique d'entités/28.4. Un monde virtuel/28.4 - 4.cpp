class A
{
public:
  // Fonction membre virtuelle.
  virtual void f1(int entier);
  // Autre fonction membre, non-virtuelle.
  void f2(int entier);
};

class B : public A
{
public:
  // Compile et fonctionne.
  void f1(int entier) override;
  // Erreur car surcharge non-virtuelle (présence de const).
  void f1(int entier) const override;
  // Erreur car non-virtuelle dans la classe parent.
  void f2(int entier) override;
};

int main()
{
  A a {};
  B b {};
  return 0;
} 