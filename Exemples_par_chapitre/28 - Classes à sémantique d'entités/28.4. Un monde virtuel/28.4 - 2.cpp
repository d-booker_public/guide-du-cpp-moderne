#include <iostream>

enum class MethodeNettoyage
{
  AvecBalai, AvecAspirateur, AvecAspirateurAuto
};

void preparer_outil(MethodeNettoyage methode)
{
  if (methode == MethodeNettoyage::AvecBalai)
  {
    std::cout << "Je saisis le balai.\n";
  }
  else if (methode == MethodeNettoyage::AvecAspirateur)
  {
    std::cout << "Je branche l'aspirateur.\n";
    std::cout << "J'allume l'aspirateur.\n";
    std::cout << "Je saisis le manche de l'aspirateur.\n";
  }
  else if (methode == MethodeNettoyage::AvecAspirateurAuto)
  {
    // On imagine une logique derrière pour connaître le niveau de charge.
    bool const est_charge { false };
    if (!est_charge)
    {
      std::cout << "Je recharge l'aspirateur automatique.\n";
    }
    std::cout << "Je branche l'aspirateur automatique.\n";
    std::cout << "Je démarre l'aspirateur automatique.\n";
  }
}

void nettoyer(MethodeNettoyage methode)
{
  if (methode == MethodeNettoyage::AvecBalai)
  {
    std::cout << "Je nettoie avec un mouvement de balancier.\n";
    std::cout << "Je me déplace.\n";
  }
  else if (methode == MethodeNettoyage::AvecAspirateur)
  {
    std::cout << "Je nettoie avec des aller-retours.\n";
    std::cout << "Je me déplace.\n";
  }
  else if (methode == MethodeNettoyage::AvecAspirateurAuto)
  {
    std::cout << "Je regarde faire.\n";
  }
}

void ranger_outil(MethodeNettoyage methode)
{
  if (methode == MethodeNettoyage::AvecBalai)
  {
    std::cout << "Je remets le balai dans le placard.\n";
  }
  else if (methode == MethodeNettoyage::AvecAspirateur)
  {
    std::cout << "J'éteins l'aspirateur.\n";
    std::cout << "Je débranche l'aspirateur.\n";
    std::cout << "Je range l'aspirateur.\n";
  }
  else if (methode == MethodeNettoyage::AvecAspirateurAuto)
  {
    std::cout << "J'éteins l'aspirateur automatique.\n";
    std::cout << "Je range l'aspirateur automatique.\n";
  }
}

void nettoyer_sol(MethodeNettoyage methode)
{
  preparer_outil(methode);
  nettoyer(methode);
  ranger_outil(methode);
}

int main()
{
  std::cout << "Je commence par le salon.\n";
  nettoyer_sol(MethodeNettoyage::AvecAspirateur);

  std::cout << "\nJe passe au garage.\n";
  nettoyer_sol(MethodeNettoyage::AvecBalai);

  return 0;
}