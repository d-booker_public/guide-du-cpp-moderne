#include <iostream>

class MethodeNettoyage
{
public:
  MethodeNettoyage() = default;
  MethodeNettoyage(MethodeNettoyage const& copie) = delete;
  MethodeNettoyage& operator=(MethodeNettoyage const& copie) = delete;
  virtual void preparer_outil() const;
  virtual void nettoyer() const;
  virtual void ranger_outil() const;
};

void MethodeNettoyage::preparer_outil() const
{
  std::cout << "Préparation de l'outil de nettoyage.\n";
}

void MethodeNettoyage::nettoyer() const
{
  std::cout << "Nettoyage de la pièce.\n";
}

void MethodeNettoyage::ranger_outil() const
{
  std::cout << "Rangement de l'outil de nettoyage.\n";
}

class AvecBalai : public MethodeNettoyage
{
public:
  void preparer_outil() const override;
  void nettoyer() const override;
  void ranger_outil() const override;
};

void AvecBalai::preparer_outil() const
{
  std::cout << "Je saisis le balai.\n";
}

void AvecBalai::nettoyer() const
{
  std::cout << "Je nettoie avec un mouvement de balancier.\n";
  std::cout << "Je me déplace.\n";
}

void AvecBalai::ranger_outil() const
{
  std::cout << "Je remets le balai dans le placard.\n";
}

class AvecAspirateur : public MethodeNettoyage
{
public:
  void preparer_outil() const override;
  void nettoyer() const override;
  void ranger_outil() const override;
};

void AvecAspirateur::preparer_outil() const
{
  std::cout << "Je branche l'aspirateur.\n";
  std::cout << "J'allume l'aspirateur.\n";
  std::cout << "Je saisis le manche de l'aspirateur.\n";
}

void AvecAspirateur::nettoyer() const
{
  std::cout << "Je nettoie avec des aller-retours.\n";
  std::cout << "Je me déplace.\n";
}

void AvecAspirateur::ranger_outil() const
{
  std::cout << "J'éteins l'aspirateur.\n";
  std::cout << "Je débranche l'aspirateur.\n";
  std::cout << "Je range l'aspirateur.\n";
}

class AvecAspirateurAuto : public MethodeNettoyage
{
public:
  void preparer_outil() const override;
  void nettoyer() const override;
  void ranger_outil() const override;
private:
  bool m_est_charge{ false };
};

void AvecAspirateurAuto::preparer_outil() const
{
  if (!m_est_charge)
  {
    std::cout << "Je recharge l'aspirateur automatique.\n";
  }
  std::cout << "Je démarre l'aspirateur automatique.\n";
}

void AvecAspirateurAuto::nettoyer() const
{
  std::cout << "Je regarde faire.\n";
}

void AvecAspirateurAuto::ranger_outil() const
{
  std::cout << "J'éteins l'aspirateur automatique.\n";
  std::cout << "Je range l'aspirateur automatique.\n";
}

void nettoyer_sol(MethodeNettoyage const & methode)
{
  methode.preparer_outil();
  methode.nettoyer();
  methode.ranger_outil();
}

int main()
{
  std::cout << "Je commence par le salon.\n";
  AvecAspirateur const pour_le_salon {};
  nettoyer_sol(pour_le_salon);

  std::cout << "\nJe passe au garage.\n";
  AvecBalai const pour_le_garage {};
  nettoyer_sol(pour_le_garage);

  std::cout << "\nMaintenant la chambre.\n";
  AvecAspirateurAuto const pour_la_chambre {};
  nettoyer_sol(pour_la_chambre);

  return 0;
}