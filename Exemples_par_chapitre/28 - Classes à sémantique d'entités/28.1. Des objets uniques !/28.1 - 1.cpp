#include <cassert>
#include <iostream>
#include <string>

class Personne
{
public:
  Personne() = delete;
  Personne(std::string const &nom, std::string const &prenom, int salaire);
  Personne(Personne const &copie) = delete;
  Personne &operator=(Personne const &copie) = delete;

  void travailler() const;

private:
  std::string m_nom;
  std::string m_prenom;
  int m_salaire;
};

Personne::Personne(std::string const &nom, std::string const &prenom, int salaire)
  : m_nom(nom), m_prenom(prenom), m_salaire(salaire)
{
  assert(!std::empty(m_nom) && "Le nom de famille est obligatoire.");
  assert(!std::empty(m_prenom) && "Le prénom est obligatoire.");
  assert(m_salaire >= 0 && "C'est vous qui payez la personne, pas l'inverse !");
}

void Personne::travailler() const
{
  std::cout << "Je suis " << m_prenom << " " << m_nom << " et j'effectue différentes tâches.\n";
}

using namespace std::literals;

int main()
{
  Personne const patron {"Ron"s, "Pat"s, 2500};
  patron.travailler();

  // Pauvre Bob...
  Personne const stagiaire {"Le Stagiaire"s, "Bob"s, 0};
  stagiaire.travailler();

  return 0;
}