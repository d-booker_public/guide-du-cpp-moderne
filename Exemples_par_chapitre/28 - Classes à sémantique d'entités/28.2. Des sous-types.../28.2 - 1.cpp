#include <cassert>
#include <iostream>
#include <string>

class Personne
{
public:
  Personne() = delete;
  Personne(std::string const &nom, std::string const &prenom, int salaire);
  Personne(Personne const &copie) = delete;
  Personne &operator=(Personne const &copie) = delete;

  void travailler() const;

private:
  std::string m_nom;
  std::string m_prenom;
  int m_salaire;
};

Personne::Personne(std::string const &nom, std::string const &prenom, int salaire)
  : m_nom(nom), m_prenom(prenom), m_salaire(salaire)
{
  assert(!std::empty(m_nom) && "Le nom de famille est obligatoire.");
  assert(!std::empty(m_prenom) && "Le prénom est obligatoire.");
  assert(m_salaire >= 0 && "C'est vous qui payez la personne, pas l'inverse !");
}

void Personne::travailler() const
{
  std::cout << "Je suis " << m_prenom << " " << m_nom << " et j'effectue différentes tâches.\n";
}

class Directeur : public Personne
{
public:
  // On demande à réexposer les constructeurs fournis 
  // dans la classe parent.
  using Personne::Personne;
  void assister_aux_reunions() const;
};

void Directeur::assister_aux_reunions() const
{
  std::cout << "Des décisions importantes doivent être prises aujourd'hui !\n";
} 

using namespace std::literals;

int main()
{
  Directeur const patron { "Ron"s, "Pat"s, 2500 };
  // Comportement hérité de Personne.
  patron.travailler();
  // Comportement propre à un Directeur.
  patron.assister_aux_reunions();

  Personne const stagiaire { "Le Stagiaire"s, "Bob"s, 0 };
  stagiaire.travailler();

  return 0;
}