#include <cassert>
#include <iostream>
#include <string>

class Personne
{
public:
  Personne() = delete;
  Personne(std::string const &nom, std::string const &prenom, int salaire);
  Personne(Personne const &copie) = delete;
  Personne &operator=(Personne const &copie) = delete;
  void se_presenter() const;
  void travailler() const;

private:
  std::string m_nom;
  std::string m_prenom;
  int m_salaire;
};

Personne::Personne(std::string const &nom, std::string const &prenom, int salaire)
  : m_nom(nom), m_prenom(prenom), m_salaire(salaire)
{
  assert(!std::empty(m_nom) && "Le nom de famille est obligatoire.");
  assert(!std::empty(m_prenom) && "Le prénom est obligatoire.");
  assert(m_salaire >= 0 && "C'est vous qui payez la personne, pas l'inverse !");
}

void Personne::se_presenter() const
{
  std::cout << "Je suis " << m_prenom << " " << m_nom << ".\n";
}

void Personne::travailler() const
{
  std::cout << "J'effectue différentes tâches.\n";
}

class Directeur : public Personne
{
public:
  Directeur(std::string const &nom, std::string const &prenom, int salaire, std::string const &direction);
  void assister_aux_reunions() const;

private:
  std::string m_direction;
};

Directeur::Directeur(std::string const &nom, std::string const &prenom, int salaire, std::string const &direction)
  : Personne(nom, prenom, salaire), m_direction(direction)
{
  assert(!std::empty(m_direction) && "On doit avoir une direction définie.");
}

void Directeur::assister_aux_reunions() const
{
  std::cout << "Des décisions importantes doivent être prises aujourd'hui pour le service " << m_direction << " !\n";
}

// Cette fonction doit fonctionner, peu importe que je lui passe une
// Personne directement, ou un sous - type.
void commencer_journee(Personne const &personne)
{
  personne.se_presenter();
  personne.travailler();
}

using namespace std::literals;

int main()
{
  Directeur const patron_dsi {"Ron"s, "Pat"s, 2500, "DSI"s};
  commencer_journee(patron_dsi);

  Directeur const patron_marketing {"Eting"s, "Mark"s, 5000, "Marketing"s};
  commencer_journee(patron_marketing);

  Personne const stagiaire {"Le Stagiaire", "Bob", 0};
  commencer_journee(stagiaire);

  return 0;
}