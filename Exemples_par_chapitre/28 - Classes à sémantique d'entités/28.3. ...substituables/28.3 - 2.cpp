#include <iostream>
#include <stdexcept>

class Oiseau
{
public:
  Oiseau() = default;
  Oiseau(Oiseau const& copie) = delete;
  Oiseau& operator=(Oiseau const& autre_oiseau) = delete;

  virtual void voler() const;
};

void Oiseau::voler() const
{
  std::cout << "Je vole !\n";
}

// Un manchot est bien un oiseau, n'est-ce-pas ?
class Manchot : public Oiseau
{
public:
  void voler() const override;
};

void Manchot::voler() const
{
  // Pourtant un manchot ne sait pas voler.
  throw std::logic_error("Non je nage ou je me dandine en marchant!");
}

void voler_gracieusement(Oiseau const & oiseau)
{
  std::cout << "Et c'est le décollage !\n";
  oiseau.voler();
}

int main()
{
  Manchot empereur {};
  voler_gracieusement(empereur);

  return 0;
}
