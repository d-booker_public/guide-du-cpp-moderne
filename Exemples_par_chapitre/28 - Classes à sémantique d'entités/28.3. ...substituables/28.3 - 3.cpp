#include <iostream>
#include <stdexcept>
#include <string>

int main()
{
  try
  {
    // Va lancer une exception std::out_of_range.
    int entier { std::stoi("1000000000000000000000000000000000000000000000000000000") };
    std::cout << "Entier : " << entier << "\n";
  }
  catch (std::exception const & exception)
  {
    std::cout << "Une erreur est survenue : " << exception.what() << "\n";
  }

  return 0;
}    