#include <iostream>

struct Base
{
  // Attribut commun à tous les descendants.
  int i;
  friend std::ostream& operator<<(std::ostream& os, Base const& valeur);
};

std::ostream& operator<<(std::ostream& os, Base const& valeur)
{
  return os << "i = " << valeur.i;
}

struct Enfant : public Base
{
  // Un attribut spécifique à Enfant.
  int j;
  friend std::ostream& operator<<(std::ostream& os, Enfant const& valeur);
};

std::ostream& operator<<(std::ostream& os, Enfant const& valeur)
{
  return os << "i = " << valeur.i << " - j = " << valeur.j;
}

int main()
{
  // On crée et on affiche deux enfants, OK.
  Enfant e1 { 1, 42 };
  Enfant e2 { 2, 84 };

  std::cout << "e1 : " << e1 << std::endl;
  std::cout << "e2 : " << e2 << std::endl;

  // Est-ce que je peux faire appel au constructeur par copie ?
  Base b1 { e1 };
  // Mais où est passé 'j' ?
  std::cout << "b1 : " << b1 << std::endl;

  // Bon, est-ce que je peux au moins avoir une référence
  // sur Base qui soit en fait un Enfant ? Oui.
  Base& b2 { e2 };
  // Ok, à travers la référence, je vais assigner une 
  // nouvelle valeur à e2.
  b2 = e1;
  // Hein ?
  std::cout << "e2 : " << e2 << std::endl;

  return 0;
}