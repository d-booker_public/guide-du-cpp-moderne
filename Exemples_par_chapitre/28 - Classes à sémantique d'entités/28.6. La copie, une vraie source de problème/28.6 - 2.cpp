#include <iostream>

struct Base
{
  int i;
};

struct Enfant : public Base
{
  int j;
};

int main()
{
  std::cout << "Taille de Base : " << sizeof(Base) << "\n";
  std::cout << "Taille de Enfant : " << sizeof(Enfant) << "\n";

  return 0;
}