#include <string>
#include <vector>

class BaseDonnees
{
public:
  BaseDonnees(std::string const & utilisateur, std::string const & mot_de_passe);
  void connexion_base_donnees();
  std::vector<int> recuperation_donnees();
};

class TransformationDonnees
{
public:
  TransformationDonnees(std::vector<int> const & donnees_brutes);
  std::vector<int> transformation();
};

class GenerationSortie
{
public:
  GenerationSortie(std::vector<int> const & donnes_transformees);
  void generation_sortie();
};