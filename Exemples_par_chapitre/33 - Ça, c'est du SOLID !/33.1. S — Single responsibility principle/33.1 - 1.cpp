#include <string>
#include <vector>

class Processus
{
public:
  void connexion_base_donnees(std::string const & utilisateur, std::string const & mot_de_passe);
  std::vector<int> recuperation_donnees();
  std::vector<int> transformation(std::vector<int> const & donnees_brutes);
  void generation_sortie(std::vector<int> const & donnees_transformees);
};