#include <iostream>

class ElementGraphique
{
public:
  virtual void copier() const noexcept = 0;
  virtual void clic() const noexcept = 0;
  virtual void afficher() const noexcept = 0;
  // ...
  // Encore d'autres services.
};

class Bouton : public ElementGraphique
{
public:
  void afficher() const noexcept override;
  void clic() const noexcept override;
  // On est forcé d'implémenter les autres fonctions membres.
  // Pourtant, elles ne nous intéressent pas.
  void copier() const noexcept override;
};

void Bouton::afficher() const noexcept
{
  std::cout << "Je suis un bouton qui s'affiche.\n";
}

void Bouton::clic() const noexcept
{
  std::cout << "On me clique dessus, je réagis.\n";
}

void Bouton::copier() const noexcept
{
  // Que mettre ?
  // Laisser le corps de la fonction vide ?
  // Lancer une exception ? Une assertion ?
}

class Label : public ElementGraphique
{
public:
  void afficher() const noexcept override;
  void copier() const noexcept override;
  // On est forcé d'implémenter les autres fonctions membres.
  // Pourtant, elles ne nous intéressent pas.
  void clic() const noexcept override;
};

void Label::afficher() const noexcept
{
  std::cout << "Je suis un label qui s'affiche.\n";
}

void Label::copier() const noexcept
{
  std::cout << "L'utilisateur copie mon contenu.\n";
}

void Label::clic() const noexcept
{
  // Même problème, que mettre ici ?
}