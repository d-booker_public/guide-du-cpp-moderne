#include <iostream>

class ElementAffichable
{
public:
  virtual void afficher() const noexcept = 0;
};

class ElementCopiable
{
public:
  virtual void copier() const noexcept = 0;
};

class ElementCliquable
{
public:
  virtual void clic() const noexcept = 0;
};

class Bouton : public ElementAffichable, public ElementCliquable
{
public:
  void afficher() const noexcept override;
  void clic() const noexcept override;
};

void Bouton::afficher() const noexcept
{
  std::cout << "Je suis un bouton qui s'affiche.\n";
}

void Bouton::clic() const noexcept
{
  std::cout << "On me clique dessus, je réagis.\n";
}

class Label : public ElementAffichable, public ElementCopiable
{
public:
  void afficher() const noexcept override;
  void copier() const noexcept override;
};

void Label::afficher() const noexcept
{
  std::cout << "Je suis un label qui s'affiche.\n";
}

void Label::copier() const noexcept
{
  std::cout << "L'utilisateur copie mon contenu.\n";
}