import java.util.Stack;

public class Exemple
{
  public static void main(String args[])
  {
    // On crée une pile de chaînes de caractères, OK.
    Stack<String> stack = new Stack<String>();

    // J'entasse les éléments un par un, OK.
    stack.push("1");
    stack.push("2");
    stack.push("3");

    // J'insère le "4" à l'emplacement 1.
    // Viol évident du LSP, BOOM !
    stack.insertElementAt("4", 1);
  }
}