#include <string>

// Service de bas niveau pour utiliser le protocole HTTP.
class ServiceHttp
{
public:
  void requete() const noexcept;
  void envoyer_reponse(std::string const & reponse) const noexcept;
};

// Service de bas niveau pour manipuler une base de données.
class BaseDonnees
{
public:
  void ecrire_donnees(int donnee) const noexcept;
};

// Service de haut niveau, qui contient la logique métier.
class TraitementEntiers
{
public:
  void traiter_reception_entier(int entier) const noexcept;

private:
  ServiceHttp m_service_http;
  BaseDonnees m_base_donnees;
};

void TraitementEntiers::traiter_reception_entier(int entier) const noexcept
{
  using namespace std::literals;
  m_base_donnees.ecrire_donnees(entier);
  m_service_http.envoyer_reponse("Bien reçu !"s);
}