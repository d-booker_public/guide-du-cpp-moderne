#include <memory>

struct IReponse
{
};

struct ReponseString : public IReponse
{
};

struct ReponseJson : public IReponse
{
};

struct IServiceHttp
{
  virtual void requete() const noexcept = 0;
  // On ne dépend plus de détails bas niveau pour répondre à une requête.
  virtual void envoyer_reponse(IReponse const& reponse) const noexcept = 0;
};

// Service bas niveau pour utiliser le protocole HTTP.
class ServiceHttp : public IServiceHttp
{
public:
  void requete() const noexcept override;
  void envoyer_reponse(IReponse const & reponse) const noexcept override;
};

struct IBaseDonnees
{
  virtual void ecrire_donnees(int donnee) const noexcept = 0;
};

// Service bas niveau pour manipuler une base de données.
class BaseDonnees : public IBaseDonnees
{
public:
  void ecrire_donnees(int donnee) const noexcept override;
};

// Service haut niveau, qui contient la logique métier.
class TraitementEntiers
{
public:
  void traiter_reception_entier(int entier) const noexcept;

private:
  // On dépend maintenant d'interfaces.
  // Notez l'utilisation de std::unique_ptr pour permettre le polymorphisme.
  std::unique_ptr<IServiceHttp> m_service_http;
  std::unique_ptr<IBaseDonnees> m_base_donnees;
};

void TraitementEntiers::traiter_reception_entier(int entier) const noexcept
{
  m_base_donnees->ecrire_donnees(entier);
  ReponseString response {};
  m_service_http->envoyer_reponse(response);
}