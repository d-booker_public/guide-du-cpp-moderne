#include <iostream>

enum class Tva
{
  Tva5Pourcent,
  Tva10Pourcent,
  Tva20Pourcent
};

struct Produit
{
  double prix;
  Tva taxe = Tva::Tva20Pourcent;
};

double prix_ttc(Produit const& produit)
{
  if (produit.taxe == Tva::Tva5Pourcent)
  {
    return produit.prix + (produit.prix * 5 / 100);
  }
  else if (produit.taxe == Tva::Tva10Pourcent)
  {
    return produit.prix + (produit.prix * 10 / 100);
  }
  else
  {
    return produit.prix + (produit.prix * 20 / 100);
  }
}

int main()
{
  Produit const voiture { 10000 };
  Produit const bois_chauffage { 100, Tva::Tva10Pourcent };
  Produit const pates { 3, Tva::Tva5Pourcent };

  std::cout << "Prix TTC de cette superbe voiture : " << prix_ttc(voiture) << std::endl;
  std::cout << "Prix TTC de ce bois pour vous réchauffer en hiver : " << prix_ttc(bois_chauffage) << std::endl;
  std::cout << "Prix TTC de ces délicieuses pâtes : " << prix_ttc(pates) << std::endl;

  return 0;
}