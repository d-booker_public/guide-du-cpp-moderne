#include <iostream>

struct Taxe5Pourcent
{
  double prix_ttc(double prix_ht) const noexcept;
};

double Taxe5Pourcent::prix_ttc(double prix_ht) const noexcept
{
  return prix_ht + (prix_ht * 5 / 100);
}

struct Taxe10Pourcent
{
  double prix_ttc(double prix_ht) const noexcept;
};

double Taxe10Pourcent::prix_ttc(double prix_ht) const noexcept
{
  return prix_ht + (prix_ht * 10 / 100);
}

struct Taxe20Pourcent
{
  double prix_ttc(double prix_ht) const noexcept;
};

double Taxe20Pourcent::prix_ttc(double prix_ht) const noexcept
{
  return prix_ht + (prix_ht * 20 / 100);
}

struct Produit
{
  double prix;
};

template <typename CalculateurTaxe>
double prix_ttc(Produit const& produit, CalculateurTaxe const & calculateur)
{
  return calculateur.prix_ttc(produit.prix);
}

int main()
{
  Taxe5Pourcent const taxe5Pourcent {};
  Taxe10Pourcent const taxe10Pourcent {};
  Taxe20Pourcent const taxe20Pourcent {};

  Produit const voiture{ 10000 };
  Produit const bois_chauffage { 100 };
  Produit const pates { 3 };
  
  std::cout << "Prix TTC de cette superbe voiture : " << prix_ttc(voiture, taxe20Pourcent) << std::endl;
  std::cout << "Prix TTC de ce bois pour vous l'hiver : " << prix_ttc(bois_chauffage, taxe10Pourcent) << std::endl;
  std::cout << "Prix TTC de ces délicieuses pâtes : " << prix_ttc(pates, taxe5Pourcent) << std::endl;

  return 0;
}