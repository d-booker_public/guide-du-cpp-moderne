#include <iostream>

int main()
{
  // Compte de 10 à 1 et de 1 à 10 en même temps.
  for (int compteur{10}, compteur_inverse{1}; compteur > 0 && compteur_inverse <= 10; --compteur, ++compteur_inverse)
  {
    std::cout << compteur << " et " << compteur_inverse << std::endl;
  }

  std::cout << "Fin du programme." << std::endl;
  return 0;
}