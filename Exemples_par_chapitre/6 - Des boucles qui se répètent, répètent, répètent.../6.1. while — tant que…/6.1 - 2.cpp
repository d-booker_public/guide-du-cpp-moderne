#include <iostream>

int main()
{
  std::cout << "Salut, je vais compter de 0 à 9." << std::endl;
  int compteur {0};

  while (compteur < 10)
  {
    std::cout << compteur << std::endl;
    // Oups, on a oublié d'incrémenter le compteur.
    // Nous sommes bloqués dans la boucle.
  }
  
  std::cout << "Je ne serai jamais affiché." << std::endl;
  return 0;
}