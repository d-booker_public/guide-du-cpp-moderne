#include <iostream>

int main()
{
  char entree {'?'};
  int compteur {0};

  // On boucle tant qu'on n'a pas rentré la lettre 'e'
  // ou que le compteur n'a pas atteint 5.
  while (compteur < 5 && entree != 'e')
  {
    std::cout << "Le compteur vaut " << compteur << std::endl;
    std::cout << "Rentre un caractère : ";
    std::cin >> entree;
    ++compteur;
  }

  std::cout << "Fin du programme." << std::endl;
  return 0;
}
