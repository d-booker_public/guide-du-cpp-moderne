#include <iostream>

int main()
{
  for (int i {0}; i < 5; ++i)
  {
    for (int j {0}; j < 3; ++j)
    {
      if (j == 1)
      {
        // Quand j vaut 1, on retourne dans le for(i).
        break;
      }

      std::cout << "Voici la valeur de j : " << j << std::endl;
    }
    
    std::cout << "Voici la valeur de i : " << i << std::endl;
  }

  return 0;
}