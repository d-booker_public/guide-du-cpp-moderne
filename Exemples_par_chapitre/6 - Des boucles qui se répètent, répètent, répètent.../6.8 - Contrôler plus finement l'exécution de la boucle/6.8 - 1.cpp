#include <iostream>

int main()
{
  for (int i {0}; i < 10; ++i)
  {
    // Si i vaut 3, j'arrête la boucle.
    if (i == 3)
    {
      break;
    }

    std::cout << i << std::endl;
  }

  return 0;
}