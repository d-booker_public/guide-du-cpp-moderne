#include <iostream>
#include <string>
#include <vector>

using namespace std::literals;

int main()
{
  std::vector<int> const nombres { 1, 2, 3, 4, 5, 6 };
  std::vector<std::string> const mots { "Mot"s, "lettres"s, "test"s };
  
  auto const lambda = []<typename T>(std::vector<T> const& tableau) -> void
  {
    std::cout << "Le tableau donné contient " << std::size(tableau) << " éléments.\n";
  };
  
  lambda(nombres);
  lambda(mots);

  return 0;
}