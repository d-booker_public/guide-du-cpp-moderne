#include <iostream>
#include <vector>

template <typename T>
void afficher(std::vector<T> const & v)
{
  for (auto const & e : v)
  {
    std::cout << e << std::endl;
  }
}

int main()
{
  std::vector<int> const tableau_entiers { 1, 3, 5, 7, 9 };
  afficher(tableau_entiers);

  std::cout << std::endl;

  std::vector<double> const tableau_reels { 1.2, 3.1415, 12.5, 2.7 };
  afficher(tableau_reels);

  std::cout << std::endl;
  
  std::vector<std::string> const tableau_chaines { "Hello", "World", "Les templates", "C'est super !" };
  afficher(tableau_chaines);
  
  return 0;
}