#include <array>
#include <iostream>
#include <list>
#include <string>
#include <vector>

void afficher(std::vector<int> const & iterable)
{
  for (auto const & e : iterable)
  {
    std::cout << e << std::endl;
  }
}
    
void afficher(std::array<double, 4> const & iterable)
{
  for (auto const & e : iterable)
  {
    std::cout << e << std::endl;
  }
}
    
void afficher(std::list<std::string> const & iterable)
{
  for (auto const & e : iterable)
  {
    std::cout << e << std::endl;
  }
}
    
void afficher(std::string const & iterable)
{
  for (auto const & e : iterable)
  {
    std::cout << e << std::endl;
  }
}
    
int main()
{
  std::vector<int> tableau_entiers { 1, 3, 5, 7, 9 };
  afficher(tableau_entiers);

  std::cout << std::endl;

  std::array<double, 4> const tableau_reels { 1.2, 3.1415, 12.5, 2.7 };
  afficher(tableau_reels);
  
  std::cout << std::endl;

  std::list<std::string> const liste_chaines { "Hello", "World", "Les templates", "C'est super !" };
  afficher(liste_chaines);
  
  std::cout << std::endl;
  
  std::string const chaine { u8"Ça marche même avec des chaînes de caractères !" };
  afficher(chaine);
  
  return 0;
}