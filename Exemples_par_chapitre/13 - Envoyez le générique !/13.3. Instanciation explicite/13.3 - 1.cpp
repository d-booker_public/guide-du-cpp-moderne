#include <iostream>
#include <limits>

template <typename T>
T entree_securisee()
{
  T variable {};
  while (!(std::cin >> variable))
  {
    std::cout << "Entrée invalide. Recommence." << std::endl;
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }
  return variable;
}

int main()
{
  std::cout << "Quel jour es-tu né ? ";
  int jour { entree_securisee() };
  
  std::cout << "Quel mois ? ";
  int mois { entree_securisee() };
  
  std::cout << "Quelle année ? ";
  int annee { entree_securisee() };

  std::cout << "Quelle taille ? ";
  double taille { entree_securisee() };

  std::cout << "Tu es né le " << jour << "/" << mois << "/" << annee << " et tu mesures " << taille << "m." << std::endl;
  return 0;
}