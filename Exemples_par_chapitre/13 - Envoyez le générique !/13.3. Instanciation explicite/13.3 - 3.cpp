#include <iostream>
#include <limits>

template <typename T>
T entree_securisee()
{
  T variable {};
  while (!(std::cin >> variable))
  {
    std::cout << "Entrée invalide. Recommence." << std::endl;
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }
  return variable;
}

template <typename T>
T addition(T x, T y)
{
  return x + y;
}

int main()
{
  std::cout << "Quel jour es-tu né ? ";
  int jour { entree_securisee<int>() };
  
  std::cout << "Quel mois ? ";
  int mois { entree_securisee<int>() };
  
  std::cout << "Quelle année ? ";
  int annee { entree_securisee<int>() };

  std::cout << "Quelle taille ? ";
  double taille { entree_securisee<double>() };

  std::cout << "Tu es né le " << jour << "/" << mois << "/" << annee << " et tu mesures " << taille << "m." << std::endl;
  std::cout << "1 + 3.1415 = " << addition<double>(1, 3.1415) << std::endl;
  return 0;
}