#include <iostream>

template <typename T>
T addition(T x, T y)
{
  return x + y;
}
    
int main()
{
  // Aucun problème, on a deux int ici.
  std::cout << addition(1, 2) << std::endl;
  // Oups. Le type déduit, c'est quoi alors ? int ou double ?
  std::cout << addition(1, 3.1415) << std::endl;

  return 0;
}