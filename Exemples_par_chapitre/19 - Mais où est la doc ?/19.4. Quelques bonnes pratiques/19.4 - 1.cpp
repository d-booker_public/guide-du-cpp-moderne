/**
* @brief Détermine si une expression est correctement parenthésée.
* @details La fonction regarde si on a bien le même nombre de parenthèses
* ouvrantes et fermantes et si celles-ci sont bien ordonnées.
* 
* @param[in] expression Une chaîne de caractères à analyser.
* 
* @returns `true` si l'expression est bien parenthesée, `false` sinon.
* 
* @note Voici un exemple d'utilisation.
* @code
* std::string const test { "((()))" };
* parentheses(test);
* @endcode 
*/
bool parentheses(std::string const & expression)
{
  // Implémentation.
}