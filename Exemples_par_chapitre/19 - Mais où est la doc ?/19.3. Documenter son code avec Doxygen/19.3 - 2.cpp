/**
* @brief Détermine si une expression est correctement parenthésée.
* @details La fonction regarde si on a bien le même nombre de 
* parenthèses ouvrantes et fermantes et si celles-ci sont bien ordonnées.
* 
* @param[in] expression Une chaîne de caractères à analyser.
* 
* @returns `true` si l'expression est bien parenthésée, `false` sinon.
*/
bool parentheses(std::string const & expression)
{
  int ouvrantes { 0 };
  int fermantes { 0 };

  for (auto const & caractere : expression)
  {
    if (caractere == '(')
    {
      ++ouvrantes;
    }
    else
    {
      ++fermantes;
    }
    if (fermantes > ouvrantes)
    {
      return false;
    }
  }
  
  return ouvrantes == fermantes;
} 