/**
* @brief Saisie sécurisée avec `std::cin`.
* @details Vérifie que la variable demandée est du bon type et respecte un prédicat donné.
* En cas de fermeture du flux d'entrée, une exception `std::runtime_error` est levée.
* En cas de type invalide, le flux d'entrée est vidé.
* En cas de prédicat non respecté, un message d'erreur est affiché sur la sortie standard.
* 
* @tparam T Le type de la valeur à récupérer.
* @tparam Predicat Un prédicat à passer pour personnaliser la vérification.
*
* @param[out] variable Une référence sur la variable de type `T` à récupérer.
* @param[in] predicat Un prédicat que l'entrée saisie doit respecter.
* 
* @pre Le type `T` doit être utilisable avec `std::cin`.
* @pre Le prédicat doit être de la forme `bool predicat(T variable)`.
* @post La variable sera initialisée correctement avec la valeur entrée.
*
* @exception std::runtime_error Si le flux d'entrée est fermé.
*/
template <typename T, typename Predicat>
void entree_securisee(T & variable, Predicat predicat)
{
  while (!(std::cin >> jour) || !predicat(variable))
  {
    if (std::cin.eof())
    {
      throw std::runtime_error("Le flux a été fermé !");
    }
    else if (std::cin.fail())
    {
      std::cout << "Entrée invalide. Recommence.\n";
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    else
    {
      std::cout << "Le prédicat n'est pas respecté !\n";
    }
  }
}
