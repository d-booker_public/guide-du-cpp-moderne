/** @enum Couleur
* @brief Liste des couleurs possibles.
* @details La liste des trois couleurs applicables à l'écran de fond du programme.
*/
enum class Couleur
{
  /// @brief Un bleu profond.
  Bleu,
  /// @brief Un rouge vif.
  Rouge,
  /// @brief Un vert magnifique.
  Vert
};

/** @struct InformationsPersonnelles
* @brief Les informations personnelles d'un membre.
* @details Permet de stocker, sous un même type, les nom/prénom ainsi que le genre et l'âge d'un membre.
* Il n'y a pas de limite à la taille du nom ou du prénom du membre.
*/
struct InformationsPersonnelles
{
  /// @brief Le prénom du membre.
  std::string prenom;
  /// @brief Le nom de famille du membre.
  std::string nom;
  /// @brief Femme ou homme ?
  std::string sexe;
  /// @brief L'âge du membre.
  int age;
};