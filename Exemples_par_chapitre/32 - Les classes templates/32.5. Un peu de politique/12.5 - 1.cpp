#include <iostream>

template <typename PolitiqueNettoyage>
class Nettoyeur
{
public:
  Nettoyeur(PolitiqueNettoyage const& politique)
    : m_politique{ politique }
  {
  }

  void nettoyer_sol()
  {
    m_politique.preparer_outil();
    m_politique.nettoyer();
    m_politique.ranger_outil();
  }

private:
  PolitiqueNettoyage m_politique;
};

class AvecBalai
{
public:
  void preparer_outil() const noexcept;
  void nettoyer() const noexcept;
  void ranger_outil() const noexcept;
};

void AvecBalai::preparer_outil() const noexcept
{
  std::cout << "Je saisi le balai.\n";
}

void AvecBalai::nettoyer() const noexcept
{
  std::cout << "Je nettoie avec un mouvement de balancier.\n";
  std::cout << "Je me déplace.\n";
}

void AvecBalai::ranger_outil() const noexcept
{
  std::cout << "Je remets le balai dans le placard.\n";
}

class AvecAspirateur
{
public:
  void preparer_outil() const noexcept;
  void nettoyer() const noexcept;
  void ranger_outil() const noexcept;
};

void AvecAspirateur::preparer_outil() const noexcept
{
  std::cout << "Je branche l'aspirateur.\n";
  std::cout << "J'allume l'aspirateur.\n";
  std::cout << "Je saisi le manche de l'aspirateur.\n";
}

void AvecAspirateur::nettoyer() const noexcept
{
  std::cout << "Je nettoie avec des aller-retours.\n";
  std::cout << "Je me déplace.\n";
}

void AvecAspirateur::ranger_outil() const noexcept
{
  std::cout << "J'éteins l'aspirateur.\n";
  std::cout << "Je débranche l'aspirateur.\n";
  std::cout << "Je range l'aspirateur.\n";
}

class AvecAspirateurAuto
{
public:
  void preparer_outil() const noexcept;
  void nettoyer() const noexcept;
  void ranger_outil() const noexcept;

private:
  bool m_est_charge { false };
};

void AvecAspirateurAuto::preparer_outil() const noexcept
{
  if (!m_est_charge)
  {
    std::cout << "Je recharge l'aspirateur automatique.\n";
  }
  std::cout << "Je démarre l'aspirateur automatique.\n";
}

void AvecAspirateurAuto::nettoyer() const noexcept
{
  std::cout << "Je regarde faire.\n";
}

void AvecAspirateurAuto::ranger_outil() const noexcept
{
  std::cout << "J'éteins l'aspirateur automatique.\n";
  std::cout << "Je range l'aspirateur automatique.\n";
}

int main()
{
  std::cout << "Je commence par le salon.\n";

  AvecAspirateur const pour_le_salon {};
  Nettoyeur<AvecAspirateur> nettoyeur_salon { pour_le_salon };
  nettoyeur_salon.nettoyer_sol();

  std::cout << "\nJe passe au garage.\n";
  AvecBalai const pour_le_garage {};
  Nettoyeur<AvecBalai> nettoyeur_garage { pour_le_garage };
  nettoyeur_garage.nettoyer_sol();

  std::cout << "\nMaintenant la chambre.\n";
  AvecAspirateurAuto const pour_la_chambre {};
  Nettoyeur<AvecAspirateurAuto> nettoyeur_chambre { pour_la_chambre };
  nettoyeur_chambre.nettoyer_sol();

  return 0;
}