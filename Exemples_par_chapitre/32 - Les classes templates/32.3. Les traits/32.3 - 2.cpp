#include <string>
#include <type_traits>

template <typename T>
class Matrice
{
  static_assert(std::is_arithmetic_v<T>, "Entiers ou flottants uniquement.");
};

int main()
{
  // Pas de problème.
  Matrice<int> const matrice_entiers {};
  Matrice<double> const matrice_reels {};

  // Ne compile pas.
  Matrice<std::string> const matrice_string {};

  return 0;
}