#include <iostream>
#include <string>
#include <type_traits>

int main()
{
  std::cout << std::boolalpha;
  // Syntaxe complète.
  std::cout << std::is_integral<int>::value << std::endl;
  // Syntaxe raccourcie.
  std::cout << std::is_integral_v<std::string> << std::endl;

  return 0;
}