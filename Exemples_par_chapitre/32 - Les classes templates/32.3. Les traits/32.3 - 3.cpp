#include <iostream>
#include <string>
#include <type_traits>

template <typename T, typename U>
auto comparer(T const& lhs, U const& rhs, double ecart_minimal = 0.001)
{
  // On vérifie que le test d'égalité est possible.
  static_assert(std::is_convertible_v<T, U> && std::is_convertible_v<U, T>, "T et U doivent être convertibles.");

  if (std::is_floating_point_v<T> || std::is_floating_point_v<U>)
  {
    return std::abs(lhs - rhs) < ecart_minimal;
  }
  else
  {
    return lhs == rhs;
  }
}

using namespace std::literals;

int main()
{
  std::cout << std::boolalpha;
  std::cout << comparer(3, 3) << std::endl;
  std::cout << comparer(3.00001, 3.00009) << std::endl;
  std::cout << comparer(3.00001, 3.00009, 0.0000001);

  // Erreur, un int et une std::string ne sont pas convertibles.
  // Si on tente de compiler, on aura l'erreur : T et U doivent être convertibles.
  //std::cout << comparer(3, "3"s) << std::endl;

  return 0;
}