#include <complex>
#include <concepts>
#include <iostream>
#include <string>

template <typename T>
concept ArithmetiqueBasique = requires (T lhs, T rhs)
{
  lhs + rhs;
  lhs - rhs;
  lhs * rhs;
  lhs / rhs;
}

template <ArithmetiqueBasique T>
T calcul(T const& premier, T const& deuxieme, T const& troisieme)
{
  return premier + deuxieme - troisieme;
}

int main()
{
  // Fonctionne pour des types natifs.
  std::cout << calcul(8, 2, 3) << std::endl;

  // Fonction pour des types créés qui implémentent les opérateurs demandés.
  using namespace std::complex_literals;
  
  std::complex<double> const z1 { 5 };
  std::complex<double> const z2 { 0. + 1i };
  std::complex<double> const z3 { -8. + -3i };
  std::cout << calcul(z1, z2, z3) << std::endl;

  // Ne fonctionne pas pour des chaînes de caractères.
  using namespace std::literals;
  std::cout << calcul("A"s, "B"s, "C"s) << std::endl;

  return 0;
}