#include <concepts>
#include <iostream>
#include <memory>
#include <string>

// Syntaxe la plus explicite, l'écriture est habituelle
// et on rajoute après le template les prérequis
// avec le mot clé 'requires' et ensuite les contraintes
// que l'on veut imposer.
template <typename T> requires std::copyable<T>
auto additionner(T const & lhs, T const & rhs)
{
  return lhs + rhs;
}

// Syntaxe intermédiaire, où le concept remplace
// le mot clé typename dans l'écriture du template.
template <std::convertible_to<double> T>
double conversion_double(T const & t) noexcept
{
  return t * 1.0;
}

// Syntaxe la plus raccourcie, le concept est utilisé 
// directement dans la liste des paramètres.
// Notez que nous sommes obligés d'utiliser auto en lieu
// et place des templates, pour garder la généricité.
auto ajouter_42(std::integral auto const & parametre) noexcept
{
  return parametre + 42;
}

int main()
{
  using namespace std::literals;

  // Exemple valide car les std::string sont concaténables.
  additionner("Du"s, "texte"s);
  // Exemple invalide car les std::unique_ptr ne sont pas concaténables.
  additionner(std::make_unique<int>(0), std::make_unique<int>(42));

  // Exemple valide car un int peut être converti.
  std::cout << conversion_double(0) << std::endl;
  // Exemple invalide car une std::string ne peut pas être convertie.
  std::cout << conversion_double("std::string"s) << std::endl;

  // Exemple valide car int est un type entier.
  std::cout << ajouter_42(0) << std::endl;
  // Exemple invalide car std::string n'est pas un type entier.
  std::cout << ajouter_42(""s) << std::endl;

  return 0;
}