#include <concepts>
#include <iostream>

template <typename T>
concept Id = requires(T parametre)
{
  // Notez que la contrainte sur le paramètre est entourée d'accolades {}.
  // La contrainte de retour est à la suite.
  // Le type de retour doit être exactement un std::size_t.
  { parametre.id() } -> std::same_as<std::size_t>;
};

struct A
{
  // Ok.
  std::size_t id() const noexcept
  {
    return 0;
  }
};

struct B
{
  // Ne marchera pas car int != std::size_t.
  int id() const noexcept
  {
    return 1;
  }
};

struct C
{
  // Ne marchera pas car par le bon nom.
  std::size_t obtenir_id() const noexcept
  {
    return 2;
  }
};

template <Id T>
void afficher_id(T const& parametre)
{
  std::cout << parametre.id() << std::endl;
}

int main()
{
  afficher_id(A{});
  afficher_id(B{});
  afficher_id(C{});

  return 0;
}
