#include <algorithm>
#include <vector>

int main()
{
  int const entier { 42 };
  std::vector<std::vector<int>> const tableau_2d;
  // Oups...
  std::find(std::cbegin(tableau_2d), std::cend(tableau_2d), entier);
  
  return 0;
}