#include <cassert>

template <typename T>
// Notez l'utilisation de final pour empêcher tout héritage à partir de cette classe.
class ObserverPtr final
{
public:
  // Un seul constructeur, par défaut initialise à nullptr.
  ObserverPtr(T* ptr = nullptr) noexcept
    : m_ptr(ptr)
  {
  }

  ~ObserverPtr() noexcept = default;

  ObserverPtr(ObserverPtr const& observer_ptr) noexcept = default;
  ObserverPtr& operator=(ObserverPtr const& observer_ptr) noexcept = default;

  ObserverPtr(ObserverPtr&& observer_ptr) noexcept = default;
  ObserverPtr& operator=(ObserverPtr&& observer_ptr) noexcept = default;

  T& operator*() const noexcept
  {
    assert(m_ptr != nullptr && "Impossible d'utiliser l'opérateur * sur un pointeur null.");
    return *m_ptr;
  }

  T* operator->() const noexcept
  {
    assert(m_ptr != nullptr && "Impossible d'utiliser l'opérateur -> sur un pointeur null.");
    return m_ptr;
  }

private:
  T* m_ptr;
};