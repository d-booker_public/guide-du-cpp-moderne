#ifndef PILE_HPP
#define PILE_HPP

#include <cassert>
#include <vector>

template <typename T>
class Pile
{
public:
  void ajouter(T const & valeur)
  {
    m_pile.push_back(valeur);
  }

  void supprimer()
  {
    assert(m_pile.size() > 0 && "Impossible de supprimer un élément d'une pile vide.");
    m_pile.pop_back();
  }

  T const & sommet() const
  {
    assert(m_pile.size() > 0 && "Impossible de récupérer le sommet d'une pile vide.");
    return m_pile[0];
  }

private:
  std::vector<T> m_pile;
};

#endif