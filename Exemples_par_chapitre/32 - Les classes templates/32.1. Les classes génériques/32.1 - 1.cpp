#ifndef PILE_HPP
#define PILE_HPP

#include <vector>

template <typename T>
class Pile
{
public:
  // Le type T est utilisable dans nos fonctions membres.
  void ajouter(T const & valeur);
  void supprimer();
  T const & sommet() const;
  
private:
  // Ainsi que dans les attributs.
  std::vector<T> m_pile;
};

#endif