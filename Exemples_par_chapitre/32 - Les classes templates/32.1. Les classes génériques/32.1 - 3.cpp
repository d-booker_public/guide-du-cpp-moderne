template <typename T>
class Pile
{
public:
  // Pas besoin de rajouter <T>.
  Pile() = default;
  ~Pile() noexcept = default;

  Pile(Pile const& pile) = default;
  Pile& operator=(Pile const & pile) = default;
  
  Pile(Pile&& pile) = default;
  Pile& operator=(Pile&& pile) = default;
}; 