#include <cmath>
#include <iostream>
#include <numeric>

struct Fraction 
{
  double valeur_reelle();
  void simplifier();
  int numerateur;
  int denominateur;
};

double Fraction::valeur_reelle() 
{
  return static_cast<double>(numerateur) / denominateur;
}

void Fraction::simplifier()
{
  int const pgcd { std::gcd(numerateur, denominateur) };
  numerateur /= pgcd;
  denominateur /= pgcd;
}

Fraction pow(Fraction const & fraction, int puissance)
{
  Fraction copie { fraction };
  copie.numerateur = std::pow(fraction.numerateur, puissance);
  copie.denominateur = std::pow(fraction.denominateur, puissance);
  return copie;
}

int main()
{
  Fraction const f1 { 5, 2 };
  std::cout << "Voici la valeur réelle de 5/2 : " << f1.valeur_reelle() << "\n"; 
  return 0;
}