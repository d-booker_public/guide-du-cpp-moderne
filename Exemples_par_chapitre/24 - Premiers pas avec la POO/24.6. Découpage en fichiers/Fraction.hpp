#ifndef FRACTION_HPP
#define FRACTION_HPP

struct Fraction 
{
  double valeur_reelle() const noexcept;
  void simplifier();
  int numerateur;
  int denominateur;
};

Fraction pow(Fraction const & fraction, int puissance);

#endif