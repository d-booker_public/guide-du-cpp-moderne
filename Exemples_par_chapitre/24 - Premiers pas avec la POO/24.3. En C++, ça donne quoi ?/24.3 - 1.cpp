#include <cassert>

int main()
{
  Fraction f1 { 5, 2 };
  assert(f1.valeur_reelle() == 2.5 && "5/2 équivaut à 2.5.");

  f1.simplifier();
  assert(f1.numerateur == 5 && f1.denominateur == 2 && "5/2 après simplification donne 5/2.");

  f1 = pow(f1, 2);
  assert(f1.numerateur == 25 && f1.denominateur == 4 && "5/2 au carré équivaut à 25/4.");

  Fraction f2 { 258, 43 };
  assert(f2.valeur_reelle() == 6 && "258/43 équivaut à 6.");

  f2.simplifier();
  assert(f2.numerateur == 6 && f2.denominateur == 1 && "258/43 après simplification donne 6/1.");

  f2 = pow(f2, 2);
  assert(f2.numerateur == 36 && f2.denominateur == 1 && "6/1 au carré équivaut à 36/1.");

  return 0;
}