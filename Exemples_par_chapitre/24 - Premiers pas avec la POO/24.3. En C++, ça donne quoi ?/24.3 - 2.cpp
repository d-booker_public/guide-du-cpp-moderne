#include <cassert>
#include <cmath>
#include <numeric>

struct Fraction 
{
  // Nos fonctions membres.
  double valeur_reelle();
  void simplifier();
  // Nos attributs.
  int numerateur;
  int denominateur;
};

double Fraction::valeur_reelle() 
{
  return static_cast<double>(numerateur) / denominateur;
}

void Fraction::simplifier()
{
  int const pgcd { std::gcd(numerateur, denominateur) };
  numerateur /= pgcd;
  denominateur /= pgcd;
}

// Déclaration et implémentation d'un service sous forme libre.
Fraction pow(Fraction const & fraction, int puissance)
{
  Fraction copie { fraction };
  copie.numerateur = std::pow(fraction.numerateur, puissance);
  copie.denominateur = std::pow(fraction.denominateur, puissance);
  return copie;
}