#include <string>

int main()
{
  // Écrivez cette ligne une seule fois.
  using namespace std::literals;
 
  // Puis vous pouvez déclarer autant de
  // chaînes de caractères que vous voulez.
  auto chaine { "Du texte."s };
  auto autre_chaine { "Texte alternatif."s };
 
  auto encore_une_chaine { "Allez, un dernier pour la route."s };
  return 0;
}
