int main()
{
  // Hop, le compilateur déduit que 'entier' est de type 'int'.
  auto entier { 0 };
  
  // Hop, le compilateur déduit que 'reel' est de type 'double'.
  auto reel { 3.1415 };

  return 0;
}