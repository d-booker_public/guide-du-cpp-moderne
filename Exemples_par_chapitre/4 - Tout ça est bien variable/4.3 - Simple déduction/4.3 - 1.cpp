int main()
{
  // Comme 0 est déjà un littéral entier,
  // pourquoi donc préciser 'int' ?
  int entier { 0 };
  
  // Comme 3.1415 est déjà un littéral flottant,
  // pourquoi donc préciser 'double' ?
  double reel { 3.1415 };
  
  return 0;
}
