#include <iostream>
#include <string>

int main()
{
  int reponse {42};
  std::cout << "La réponse à la Grande Question est " << reponse << std::endl;

  double pi {3.1415926};
  std::cout << "Voici la valeur du célèbre nombre pi : " << pi << std::endl;

  char lettre {'A'};
  std::cout << "La première lettre de l'alphabet français est " << lettre << std::endl;

  std::string phrase {"Bonjour tout le monde !"};
  std::cout << "En entrant dans la salle, il s'écria : " << phrase << std::endl;

  return 0;
}
