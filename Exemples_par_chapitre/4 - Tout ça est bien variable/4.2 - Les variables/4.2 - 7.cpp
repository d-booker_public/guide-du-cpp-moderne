#include <iostream>

int main()
{
  int entier { 4 };
  std::cout << "Mon entier vaut : " << entier << std::endl;
  entier = entier + 2;
  std::cout << "Mon entier vaut : " << entier << std::endl;
  
  entier = entier - 2;
  std::cout << "Mon entier vaut : " << entier << std::endl;
  
  entier = entier * 2;
  std::cout << "Mon entier vaut : " << entier << std::endl;
  
  entier = entier / 2;
  std::cout << "Mon entier vaut : " << entier << std::endl;
  
  return 0;
}
