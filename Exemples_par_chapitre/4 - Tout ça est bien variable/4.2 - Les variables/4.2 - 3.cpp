#include <iostream>

int main()
{
  // Bon exemple.
  int avec_underscore {0};
  // Erreur : espace interdit.
  int avec espace {0};

  // Bon exemple.
  int variable1 {42};
  // Erreur : ne peut pas commencer par un chiffre.
  int 1variable {42};

  // Bon exemple.
  char lettre {'A'};
  // Erreur : ponctuation interdite.
  char autre_lettre! {'B'};

  // Bon exemple.
  double retour {2.71};
  // Erreur : mot clé réservé par C++.
  double return {2.71};

  return 0;
}