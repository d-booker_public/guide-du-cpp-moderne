#include <iostream>

int main()
{
  int entier { 4 };
  std::cout << "Mon entier vaut : " << entier << std::endl;
  
  // On peut tout à fait changer la valeur de la variable.
  entier = 4 * (8 + 9) - 1;
  std::cout << "Finalement non, il vaut : " << entier << std::endl;
  
  // On peut même utiliser la valeur de la variable
  // et la réaffecter à la même variable.
  entier = entier + 7;
  std::cout << "Et si j'additionne 7 ? " << entier << std::endl;
  
  int autre_entier { entier * 2 };
  // On peut utiliser d'autres variables également.
  entier = autre_entier - 4;
  std::cout << "Finalement, je change de valeur : " << entier << std::endl;
  
  return 0;
}
