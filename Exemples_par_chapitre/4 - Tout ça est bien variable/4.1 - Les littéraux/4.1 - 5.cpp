#include <iostream>

int main()
{
  std::cout << -1 << std::endl;
  std::cout << 0 << std::endl;
  std::cout << 1 << std::endl;
  std::cout << -1.6027 << std::endl;
  std::cout << 3.14159 << std::endl;
  std::cout << 2.71828 << std::endl;

  return 0;
}