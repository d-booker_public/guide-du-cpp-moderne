#include <iostream>

int main()
{
  // Un caractère peut être une lettre.
  std::cout << 'A' << std::endl;
  // Ou bien un chiffre.
  std::cout << '7' << std::endl;
  // Ou même de la ponctuation.
  std::cout << '!' << std::endl;
 
  return 0;
} 