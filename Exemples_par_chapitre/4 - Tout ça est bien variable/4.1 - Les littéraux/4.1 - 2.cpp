#include <iostream>

int main()
{
  // Que c'est fastidieux d'écrire ça !
  std::cout << 'S' << 'a'
  << 'l' << 'u' << 't'
  << ' ' << 't' << 'o'
  << 'i' << ' ' << '!'
  << std::endl;
  
  // C'est tellement mieux comme ça.
  std::cout << "Salut toi !" << std::endl;
  
  return 0;
} 