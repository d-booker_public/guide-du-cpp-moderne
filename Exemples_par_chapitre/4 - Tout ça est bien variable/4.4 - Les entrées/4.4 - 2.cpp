#include <iostream>
#include <string>

int main()
{
  std::cout << "Entre ton age : ";
  int age { 0 };
  std::cin >> age;
  std::cout << "Tu as " << age << " ans.\n";
 
  std::cout << "Entre ton nom : ";
  std::string nom { "" };
  std::cin >> nom;
  std::cout << "Tu t'appelles " << nom << ".\n";
  return 0;
}
