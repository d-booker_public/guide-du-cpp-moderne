void fonction(int parametre)
{
  // Aucun problème.
  parametre = 5;
}

int main()
{
  // La variable parametre n'existe pas !
  parametre = 410;
  return 0;
}