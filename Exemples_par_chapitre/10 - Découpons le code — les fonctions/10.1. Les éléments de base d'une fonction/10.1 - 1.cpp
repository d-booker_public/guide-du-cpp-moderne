#include <iostream>

int pgcd(int a, int b)
{
  int r { a % b };
  while (r != 0)
  {
    a = b;
    b = r;
    r = a % b;
  }
  // On peut tout à fait renvoyer la valeur d'un paramètre.
  return b;
}

int somme(int n)
{
  // On peut renvoyer le résultat d'un calcul
  // ou d'une expression directement.
  return (n * (n + 1)) / 2;
}

bool ou_exclusif(bool a, bool b)
{
  return (a && !b) || (b && !a);
}

int main()
{
  int const a { 845 };
  int const b { 314 };

  std::cout << "Le PGCD de " << a << " et " << b << " vaut " << pgcd(a, b) << "." << std::endl;
  std::cout << "La somme de tous les entiers de 1 à 25 vaut " << somme(25) << "." << std::endl;
  std::cout << std::boolalpha;
  std::cout << "XOR(true, false) vaut " << ou_exclusif(true, false) << "." << std::endl;
  
  return 0;
}