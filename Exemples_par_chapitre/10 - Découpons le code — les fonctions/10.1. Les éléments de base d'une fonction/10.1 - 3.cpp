void fonction(int entier) 
{ 
  // entier est le paramètre de la fonction. 
} 
 
int main() 
{ 
  // 4 est l'argument de la fonction. 
  fonction(4); 
 
  int const valeur { 5 }; 
  // valeur est l'argument de la fonction. 
  fonction(valeur); 

  return 0; 
}