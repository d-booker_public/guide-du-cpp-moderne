#include <iostream>
#include <vector>

void fonction(int a, double b, std::vector<int> c)
{
  a = 5;
  b = 2.18781;
  c.assign(7, 0);
  // Ici, nous manipulons donc des copies.
  std::cout << a << std::endl;
  std::cout << b << std::endl;
  for (auto element : c)
  {
    std::cout << element << " ";
  }

  std::cout << std::endl << std::endl;
}

int main()
{
  int const entier { 42 };
  double const reel { 3.14159 };
  std::vector<int> const tableau { 1, 782, 4, 5, 71 };

  // Chaque variable va être copiée.
  fonction(entier, reel, tableau);
  
  // Affichons pour prouver que les variables originales n'ont pas changé.
  std::cout << entier << std::endl;
  std::cout << reel << std::endl;
  for (auto element : tableau)
  {
    std::cout << element << " ";
  }
  
  std::cout << std::endl;
  return 0;
}