#include <iostream>

void test(int & a)
{
  std::cout << "Je suis dans la fonction test.\n";
}
    
void test_const(int const & a)
{
  std::cout << "Je suis dans la fonction test_const.\n";
}

int main()
{
  test(42); 
  test_const(42);

  return 0;
}