int main()
{
  int variable { 2 };
  int & reference { variable };

  // Sera toujours une variable et jamais une référence.
  auto toujours_variable_1 { variable };
  auto toujours_variable_2 { reference };

  // Sera toujours une référence et jamais une variable.
  auto & toujours_reference_1 { variable };
  auto & toujours_reference_2 { reference };

  // On pourrait rajouter des exemples avec const mais vous comprenez le principe.
  return 0;
}