void fonction(int a)
{
  a = 5;
}

int main()
{
  int a { 8 };
  
  // Aucun problème.
  fonction(a);

  return 0;
}