int& fonction()
{
  int variable { 42 };
  // Ohoh, variable sera détruite quand la fonction sera terminée !
  return variable;
}
    
int main()
{
  // Grosse erreur, notre référence est invalide !
  int& reference { fonction() };
  return 0;
}