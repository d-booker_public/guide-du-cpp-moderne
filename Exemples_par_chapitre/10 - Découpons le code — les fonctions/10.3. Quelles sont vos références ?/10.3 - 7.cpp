void fonction(int & reference)
{
  // On modifie une référence sur une variable qui existe, c'est OK.
  reference = 42;
}
    
int main()
{
  int variable { 24 };
  // On transmet variable...
  fonction(variable);
  // ... qui existe toujours après la fin de la fonction.

  return 0;
}