#include <iostream>

int main()
{
  int entier { 40 };
  // On initialise notre référence pour qu'elle référence entier.
  int & reference_entier { entier };
  std::cout << "Entier vaut : " << entier << std::endl;
  std::cout << "Référence vaut : " << reference_entier << std::endl;

  reference_entier += 2;
  std::cout << "Entier vaut : " << entier << std::endl;
  std::cout << "Référence vaut : " << reference_entier << std::endl;

  int const entier_constant { 0 };
  // On peut également déclarer des références constantes
  // sur des entiers constants.
  int const & reference_const_entier_constant { entier_constant };
  // Ce qui interdit ce qui suit car le type référencé est un int const.
  // reference_const_entier_constant = 1;

  // On ne peut pas déclarer de référence non constante sur un entier constant.
  // int & reference_entier_constant { entier_constant };

  // On peut par contre tout à fait déclarer une référence constante sur un objet non constant.
  // On ne peut donc pas modifier la valeur de l'objet en utilisant la référence,
  // mais l'entier original reste tout à fait modifiable.
  int const & reference_const_entier { entier };
  std::cout << "Entier vaut : " << entier << std::endl;
  std::cout << "Référence constante vaut : " << reference_const_entier << std::endl;

  entier = 8;
  std::cout << "Entier vaut : " << entier << std::endl;
  std::cout << "Référence constante vaut : " << reference_const_entier << std::endl;

  return 0;
}