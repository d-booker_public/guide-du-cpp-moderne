#include <iostream>
#include <string>

int main()
{
  int const entier { std::stoi("-8") };
  double const flottant { std::stod("2.71878") };
  std::cout << entier + flottant << std::endl;

  return 0;
}