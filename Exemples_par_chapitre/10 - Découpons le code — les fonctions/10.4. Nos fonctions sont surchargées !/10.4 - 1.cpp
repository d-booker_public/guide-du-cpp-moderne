#include <iostream>
#include <string>

int main()
{
  int const entier { -5 };
  double const flottant { 3.14156 };

  std::string affichage { "Voici des nombres : " };
  affichage += std::to_string(entier) + ", ";
  affichage += std::to_string(flottant);
  std::cout << affichage << std::endl;

  return 0;
}