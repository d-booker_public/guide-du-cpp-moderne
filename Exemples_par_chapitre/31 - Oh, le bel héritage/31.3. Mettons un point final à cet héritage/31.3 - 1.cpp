class Mere
{
public:
  // Peut être redéfinie.
  virtual void fonction() const noexcept;
  // Peut être redéfinie aussi.
  virtual void autre_fonction() const noexcept;
};

class Fille : public Mere {
public:
  // redéfinie, mais qui ne  pourra plus l'être dans les classes descendantes.
  void fonction() const noexcept override final;
  // Possible.
  void autre_fonction() const noexcept override;
};

class PetiteFille : public Fille {
public:
  // Impossible !
  void fonction() const noexcept override;
  // Toujours possible.
  void autre_fonction() const noexcept override;
};