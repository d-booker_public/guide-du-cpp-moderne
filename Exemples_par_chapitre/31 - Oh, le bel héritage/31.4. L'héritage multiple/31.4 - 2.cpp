#include <iostream>

class A
{
public:
  int entier { 42 };
};

class B : public A
{
};

class C : public A
{
};

class D : public B, public C
{
};

int main()
{
  D const d {};
  std::cout << d.entier << std::endl;

  return 0;
} 