class VehiculeElectrique
{
public:
  void rouler_en_utilisant_la_batterie() const noexcept;
};

class VehiculeEssence
{
public:
  void rouler_en_utilisant_le_carburant() const noexcept;
};

// Notez la répétition de public, ce qui est important pour utiliser l'héritage public.
class VehiculeHybride : public VehiculeElectrique, public VehiculeEssence
{
};