#include <iostream>
      
class Mere
{
public:
  virtual void dire_bonjour() const noexcept;

  // Accessible à tout le monde.
  int a;

protected:
  // Accessible seulement aux instances de Mere,
  // de Fille et même de PetiteFille.
  int b;

private:
  // Accessible seulement aux instances de Mere.
  int c;
};

void Mere::dire_bonjour() const noexcept
{
  std::cout << "a = " << a << std::endl;
  std::cout << "b = " << b << std::endl;
  std::cout << "c = " << c << std::endl;
}

class Fille : public Mere
{
public:
  void dire_bonjour() const noexcept override;
};

void Fille::dire_bonjour() const noexcept
{
  // Possible car public.
  std::cout << "a = " << a << std::endl;
  // Possible car protected.
  std::cout << "b = " << b << std::endl;
  // Impossible car private.
  std::cout << "c = " << c << std::endl;
}

class PetiteFille : public Fille
{
public:
  void dire_bonjour() const noexcept override;
};

void PetiteFille::dire_bonjour() const noexcept
{
  // Possible car public.
  std::cout << "a = " << a << std::endl;
  // Possible car protected marche avec les héritages successifs.
  std::cout << "b = " << b << std::endl;
  // Impossible car private dès la classe de base.
  std::cout << "c = " << c << std::endl;
}