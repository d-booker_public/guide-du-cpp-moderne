#include <cassert>
#include <iostream>
#include <string>

struct InformationsPersonnelles
{
  std::string prenom;
  std::string nom;
  int age;
};

class Serializer
{
public:
  void ecrire(std::ostream & output,
    InformationsPersonnelles const & informations) const;

private:
  // Notez que l'écriture en elle-même est virtuelle pure car variant en fonction des sous-types.
  virtual void ecrire_implementation(
    std::ostream & output,
    InformationsPersonnelles const & informations) const = 0;
};

void Serializer::ecrire(std::ostream & output, InformationsPersonnelles const & informations) const
{
  // Préconditions : que les données soient cohérentes.
  assert(!std::empty(informations.prenom) && "Le prénom doit être renseigné.");
  assert(!std::empty(informations.nom) && "Le nom doit être renseigné.");
  assert(informations.age > 0 && "L'âge doit être correct.");

  ecrire_implementation(output, informations);
}

class XmlSerializer : public Serializer
{
private:
  void ecrire_implementation(std::ostream & output,
  InformationsPersonnelles const & informations) const override;
};

void XmlSerializer::ecrire_implementation(std::ostream & output, InformationsPersonnelles const & informations) const
{
  output << "<information>\n";
  output << "\t<prenom>" << informations.prenom << "</prenom>\n";
  output << "\t<nom>" << informations.nom << "</nom>\n";
  output << "\t<age>" << informations.age << "</age>\n";
  output << "</information>";
}

class JsonSerializer : public Serializer
{
private:
  void ecrire_implementation(std::ostream & output, InformationsPersonnelles const & informations) const override;
};

void JsonSerializer::ecrire_implementation(std::ostream & output, InformationsPersonnelles const & informations) const
{
  output << "{\n";
  output << "\t" << R"("prenom": )" << informations.prenom << ",\n";
  output << "\t" << R"("nom": )" << informations.nom << ",\n";
  output << "\t" << R"("age": )" << informations.age << "\n";
  output << "}";
}

using namespace std::literals;

void serialiser_donnees(Serializer const & serializer, InformationsPersonnelles const & informations)
{
  serializer.ecrire(std::cout, informations);
}

int main()
{
  InformationsPersonnelles const nicky_larson { "Nicky"s, "Larson"s, 20 };
  XmlSerializer const xml {};
  std::cout << "En XML : \n";
  serialiser_donnees(xml, nicky_larson);

  std::cout << "\n\n";

  InformationsPersonnelles const samus_aran { "Samus"s, "Aran"s, 20 };
  JsonSerializer const json {};
  std::cout << "En JSON : \n";
  serialiser_donnees(json, samus_aran);

  return 0;
}