#include <cassert>
#include <iostream>

struct Bagage
{
  int poids;
  int hauteur;
  int largeur;
  int profondeur;
};

class CompagnieAerienne
{
public:
  void enregistrer_bagage(Bagage const& bagage) const noexcept;

private:
  virtual void verifier_taille_bagage(Bagage const& bagage) const noexcept;
};

void CompagnieAerienne::enregistrer_bagage(Bagage const & bagage) const noexcept
{
  // Vérification des invariants à travers une
  // fonction membre éventuellement supplantable.
  verifier_taille_bagage(bagage);
  std::cout << "Bagage bien enregistré.\n";
}

void CompagnieAerienne::verifier_taille_bagage(Bagage const & bagage) const noexcept
{
  assert(bagage.poids <= 8 && "Un bagage cabine ne peut pas dépasser 8kg.");
  assert(bagage.hauteur <= 55 && bagage.largeur <= 40 && bagage.profondeur <= 20 && "Dimensions autorisées : 55x40x20");
}

class CompagnieTraditionnelle : public CompagnieAerienne
{
private:
  void verifier_taille_bagage(Bagage const & bagage) const noexcept override;
};

void CompagnieTraditionnelle::verifier_taille_bagage(Bagage const & bagage) const noexcept
{
  // Préconditions affaiblies dans un sous-type, en respect avec le LSP.
  assert(bagage.poids <= 12 && "Un bagage cabine ne peut pas dépasser 12kg.");
  assert(bagage.hauteur <= 55 && bagage.largeur <= 40 && bagage.profondeur <= 25 && "Dimensions autorisées : 55x40x25");
}

int main()
{
  Bagage const bagage { 10, 50, 40, 25 };
  CompagnieTraditionnelle const compagnie {};
  compagnie.enregistrer_bagage(bagage);

  return 0;
}