#include <iostream>
#include <vector>

void fonction()
{
  std::vector<int> tableau {};
  
  tableau.push_back(10);
  tableau.push_back(20);

  int & a { tableau.front() };
  int & b { tableau.back() };

  std::cout << "Front : " << tableau.front() << std::endl;
  std::cout << "Back : " << tableau.back() << std::endl;

  b = 5;
  
  std::cout << "Front : " << tableau.front() << std::endl;
  std::cout << "Back : " << tableau.back() << std::endl;
}
    
int main()
{
  fonction();
  return 0;
}