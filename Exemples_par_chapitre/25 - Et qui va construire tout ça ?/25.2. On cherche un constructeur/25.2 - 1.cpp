#include <cmath>
#include <iostream>
#include <numeric>

class Fraction 
{
public:
  double valeur_reelle() const noexcept;
  void simplifier();

private:
  int m_numerateur;
  int m_denominateur;
};

double Fraction::valeur_reelle() const noexcept
{
  return static_cast<double>(m_numerateur) / m_denominateur;
}

void Fraction::simplifier()
{
  int const pgcd { std::gcd(m_numerateur, m_denominateur) };
  m_numerateur /= pgcd;
  m_denominateur /= pgcd;
}

int main()
{
  Fraction const f1 { 5, 2 };
  std::cout << "5/2 = " << f1.valeur_reelle() << "\n";
  return 0;  
}