#include <cmath>
#include <iostream>
#include <numeric>

struct Fraction 
{
  double valeur_reelle() const noexcept;
  void simplifier();
  int numerateur;
  int denominateur;
};

double Fraction::valeur_reelle() const noexcept
{
  return static_cast<double>(numerateur) / denominateur;
}

void Fraction::simplifier()
{
  int const pgcd { std::gcd(numerateur, denominateur) };
  numerateur /= pgcd;
  denominateur /= pgcd;
}

Fraction pow(Fraction const & fraction, int puissance)
{
  Fraction copie { fraction };
  copie.numerateur = std::pow(fraction.numerateur, puissance);
  copie.denominateur = std::pow(fraction.denominateur, puissance);
  return copie;
}

int main()
{
  Fraction f1 { 4, 2 };
  std::cout << "4/2 = " << f1.valeur_reelle() << std::endl;
  // Erreur !
  f1.denominateur = 0;
  // Ça va faire boom !
  std::cout << "4/0 = " << f1.valeur_reelle() << std::endl;
  return 0;
}