#include <iostream>

class Fraction
{
public:
  Fraction() = delete;
  double valeur_reelle() const noexcept;
  void simplifier();

private:
  int m_numerateur;
  int m_denominateur;
};

double Fraction::valeur_reelle() const noexcept
{
  return static_cast<double>(m_numerateur) / m_denominateur;
}

void Fraction::simplifier()
{
  int const pgcd { std::gcd(m_numerateur, m_denominateur) };
  m_numerateur /= pgcd;
  m_denominateur /= pgcd;
}

int main()
{
  Fraction const f1 {};
  std::cout << "Valeur réelle par défaut = " << f1.valeur_reelle() << "\n";
  return 0;  
}