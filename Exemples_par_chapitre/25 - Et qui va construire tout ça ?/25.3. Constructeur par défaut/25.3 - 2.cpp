#include <iostream>

class Fraction
{
public:
  Fraction() = default;
  double valeur_reelle() const noexcept;
  void simplifier();

private:
  // On peut laisser le compilateur le mettre à 0, ou l'écrire nous-mêmes.
  int m_numerateur = 0;
  int m_denominateur = 1;
};

double Fraction::valeur_reelle() const noexcept
{
  return static_cast<double>(m_numerateur) / m_denominateur;
}

void Fraction::simplifier()
{
  int const pgcd { std::gcd(m_numerateur, m_denominateur) };
  m_numerateur /= pgcd;
  m_denominateur /= pgcd;
}

int main()
{
  Fraction const f1 {};
  std::cout << "Valeur réelle par défaut = " << f1.valeur_reelle() << "\n";
  return 0;  
}