#include <cassert>
#include <cmath>
#include <iostream>
#include <numeric>

class Fraction 
{
public:
  Fraction(int numerateur, int denominateur) noexcept;
  double valeur_reelle() const noexcept;
  void simplifier();

private:
  int m_numerateur;
  int m_denominateur;
};

Fraction::Fraction(int numerateur, int denominateur) noexcept
  : m_numerateur(numerateur), m_denominateur(denominateur)
{
  assert(m_denominateur != 0 && "Le dénominateur ne peut pas valoir 0.");
}

double Fraction::valeur_reelle() const noexcept
{
  return static_cast<double>(m_numerateur) / m_denominateur;
}

void Fraction::simplifier()
{
  int const pgcd { std::gcd(m_numerateur, m_denominateur) };
  m_numerateur /= pgcd;
  m_denominateur /= pgcd;
}

Fraction pow(Fraction const & fraction, int puissance)
{
  Fraction copie { fraction };
  copie.m_numerateur = std::pow(fraction.m_numerateur, puissance);
  copie.m_denominateur = std::pow(fraction.m_denominateur, puissance);
  return copie;
}

int main()
{
  Fraction const f1 { 5, 2 };
  Fraction const f2 { pow(f1, 3) }; 
  return 0;
}