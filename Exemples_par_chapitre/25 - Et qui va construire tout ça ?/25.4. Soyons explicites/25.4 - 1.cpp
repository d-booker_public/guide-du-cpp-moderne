#include <string>

class Mot
{
public:
  Mot(std::string const & mot);

private:
  std::string m_mot;
};

Mot::Mot(std::string const & mot)
  : m_mot(mot)
{
}

using namespace std::string_literals;

int main()
{
  // Conversion implicite d'une chaîne de caractères C
  // en std::string au moment de l'appel au constructeur.
  Mot const arbre { "arbre" };
  // Pas de conversion car argument de type std::string.
  Mot const agrume { "agrume"s };

  return 0;  
}