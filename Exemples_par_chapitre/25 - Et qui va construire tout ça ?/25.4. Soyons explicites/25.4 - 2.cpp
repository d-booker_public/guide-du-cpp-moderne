#include <cassert>
#include <iostream>

enum class ModeLivraison
{
  Standard,
  Express,
  Urgent,
  Economique
};

class Adresse
{
  // Juste pour l'exemple.
};

class Colis
{
public:
  Colis(double poids);

private:
  double m_poids { 0. };
};

Colis::Colis(double poids)
  : m_poids(poids)
{
  assert(poids >= 0.1 && "Le poids ne peut pas être inférieur à 100g.");
}

double calcul_prix(Colis const & colis, Adresse const & adresse, ModeLivraison mode) 
{
  // Plein d'opérations complexes pour
  // calculer le prix à payer pour le colis.
  return 10;
}

int main()
{
  Adresse adresse_livraison {};
  std::cout << "Voici le prix à payer : " << calcul_prix(4, adresse_livraison, ModeLivraison::Express) << "\n";
  return 0;
}