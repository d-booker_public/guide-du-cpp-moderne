#include <iostream>
#include <string>

int main()
{
  std::string const phrase {"Bonjour tout le monde !"};
  
  std::cout << "Première lettre : " << phrase.front() << std::endl;
  std::cout << "Dernière lettre : " << phrase.back() << std::endl;

  return 0;
}
