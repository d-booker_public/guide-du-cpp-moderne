#include <iostream>
#include <string>

int main()
{
  std::string const texte {"Du texte."};
  std::cout << std::boolalpha;
  std::cout << "Est ce que 'texte' est vide ? " << std::empty(texte) << std::endl;

  std::string const rien{""};
  std::cout << "Est ce que 'rien' est vide ? " << std::empty(rien) << std::endl;
  
  return 0;
}