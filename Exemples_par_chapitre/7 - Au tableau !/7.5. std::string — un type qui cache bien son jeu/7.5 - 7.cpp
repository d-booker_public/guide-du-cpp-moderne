#include <cassert>
#include <iostream>
#include <string>

int main()
{
  std::string phrase{"Blablabla du texte."};
  phrase.clear();
  std::cout << "Rien ne va s'afficher : " << phrase << std::endl;
  return 0;
}