#include <cassert>
#include <iostream>
#include <string>

int main()
{
  std::string const phrase {"Voici une phrase normale."};
  std::cout << "Voici la longueur de cette phrase : " << std::size(phrase) << " caractères." << std::endl;

  return 0;
}