#include <iostream>
#include <string>

int main()
{
  std::string const phrase {"Voici un ensemble de lettres."};
  
  for (char lettre : phrase)
  {
    std::cout << lettre << std::endl;
  }

  return 0;
}