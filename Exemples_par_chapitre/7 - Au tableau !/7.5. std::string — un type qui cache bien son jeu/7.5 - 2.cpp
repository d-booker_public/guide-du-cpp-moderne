#include <cassert>
#include <iostream>
#include <string>

int main()
{
  std::string const phrase {"Ceci me servira d'exemple."};
  auto const indice {5};
  
  std::cout << "Voici le caractère à la position " << indice << " : " << phrase[indice] << std::endl;
  return 0;
}
