#include <iostream>
#include <string>

int main()
{
  std::string verbe {"Estamper"};
  verbe[3] = 'o';
  std::cout << "Verbe choisi : " << verbe << std::endl;

  return 0;
}