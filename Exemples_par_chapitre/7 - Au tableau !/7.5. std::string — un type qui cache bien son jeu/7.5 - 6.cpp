#include <iostream>
#include <string>

int main()
{
  std::string phrase {"Une phrase !"};

  phrase.pop_back();
  phrase.pop_back();
  phrase.push_back('.');

  std::cout << phrase << std::endl;
  return 0;
}
