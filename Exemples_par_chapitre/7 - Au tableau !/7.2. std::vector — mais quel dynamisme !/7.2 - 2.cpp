#include <string>
#include <vector>

int main()
{
  // On stocke 5 entiers.
  std::vector<int> const tableau_de_int {1, 2, 3, 4, 5};
  // Et ici 2 flottants.
  std::vector<double> const tableau_de_double {2.71828, 3.14159};
  // Maintenant 3 chaînes de caractères.
  std::vector<std::string> const tableau_de_string {"Une phrase.", "Et une autre !", "Allez, une pour la fin."};

  return 0;
}
