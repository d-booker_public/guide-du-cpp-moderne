#include <iostream>
#include <vector>

int main()
{
  std::vector<int> tableau_de_int {12, 24};

  std::cout << "Taille avant l'ajout : " << tableau_de_int.size() << std::endl;
  tableau_de_int.push_back(36);
  tableau_de_int.push_back(48);
  tableau_de_int.push_back(100);
  std::cout << "Taille après l'ajout : " << std::size(tableau_de_int) << std::endl;

  return 0;
}
