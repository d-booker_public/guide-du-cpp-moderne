#include <iostream>
#include <vector>

int main()
{
  std::vector<int> const tableau_de_int {1, 2, 3};
  
  std::cout << "Le premier élément est " << tableau_de_int[0] << "." << std::endl;
  std::cout << "Le deuxième élément est " << tableau_de_int[1] << "." << std::endl;
  std::cout << "Le troisième élément est " << tableau_de_int[2] << "." << std::endl;

  return 0;
}

