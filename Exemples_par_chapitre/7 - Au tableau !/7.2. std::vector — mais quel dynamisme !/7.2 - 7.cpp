#include <iostream>
#include <vector>

int main()
{
  std::vector<int> const tableau_de_int {1, 2, 3};
  
  auto const taille{std::size(tableau_de_int)};
  std::cout << "Mon tableau contient " << taille << " éléments." << std::endl;
  
  return 0;
}
