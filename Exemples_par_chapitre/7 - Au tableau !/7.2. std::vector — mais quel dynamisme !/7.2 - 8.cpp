#include <iostream>
#include <vector>

int main()
{
  std::vector<int> const tableau_entiers {1, 2, 3};

  std::cout << "On va afficher le tableau en entier.\n";
  for (auto const element : tableau_entiers)
  {
    std::cout << element << std::endl;
  }

  return 0;
}
