#include <vector>
#include <iostream>

int main()
{
  std::vector<int> const nombres {2, 4, 6, 8};
  std::cout << "Deuxième élément: " << nombres[1] << '\n';
  return 0;
}
