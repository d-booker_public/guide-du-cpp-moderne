#include <iostream>
#include <vector>

int main()
{
  std::vector<int> tableau {1, 2, 3};
  
  // Finalement, on aimerait que la première valeur soit un 15.
  tableau[0] = 15;

  for (auto const valeur : tableau)
  {
    std::cout << valeur << std::endl;
  }

  return 0;
}