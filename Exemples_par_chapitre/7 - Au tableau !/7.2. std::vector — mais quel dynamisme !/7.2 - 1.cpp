#include <string>
// N'oubliez pas cette ligne.
#include <vector>

int main()
{
  std::vector<int> const tableau_de_int {};
  std::vector<double> const tableau_de_double {};
  // Même avec des chaînes de caractères c'est possible.
  std::vector<std::string> const tableau_de_string {};

  return 0;
}