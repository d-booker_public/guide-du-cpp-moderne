#include <vector>

int main()
{
  std::vector<int> const tableau_de_int {1, 2, 3};
  // Maintenant, copie possède les mêmes éléments que tableau_de_int.
  std::vector<int> const copie {tableau_de_int};

  return 0;
}