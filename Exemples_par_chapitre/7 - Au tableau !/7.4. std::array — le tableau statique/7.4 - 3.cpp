#include <array>
#include <iostream>

int main()
{
  std::array<double, 7> tableau_de_7_decimaux {3.14159, 2.1878};

  // C'est le nombre d'or.
  tableau_de_7_decimaux[2] = 1.61803;

  // Aïe aïe aïe, catastrophe !
  //tableau_de_7_decimaux[7] = 1.0;

  for (auto const element : tableau_de_7_decimaux)
  {
    std::cout << element << std::endl;
  }

  return 0;
}
