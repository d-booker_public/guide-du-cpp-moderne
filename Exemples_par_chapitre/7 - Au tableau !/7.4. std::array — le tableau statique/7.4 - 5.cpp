#include <array>
#include <iostream>

int main()
{
  std::array<int, 5> const valeurs{1, 2, 3, 4, 5};
  std::cout << "Et la taille de mon tableau vaut bien " << std::size(valeurs) << "." << std::endl;

  return 0;
}
