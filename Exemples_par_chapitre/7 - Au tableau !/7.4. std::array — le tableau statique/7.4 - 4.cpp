#include <array>
#include <iostream>

int main()
{
  std::array<int, 9> const tableau_de_int{1, 2, 3, 4, 5, 6, 7, 8, 9};

  std::cout << "Le premier élément est " << tableau_de_int.front() << "." << std::endl;
  std::cout << "Le dernier élément est " << tableau_de_int.back() << "." << std::endl;

  return 0;
}