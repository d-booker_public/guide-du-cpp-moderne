#include <array>
#include <iostream>

int main()
{
  std::array<int, 5> const valeurs {1, 2, 3, 4, 5};

  std::cout << std::boolalpha;
  std::cout << "Est-ce que mon tableau est vide ? " << std::empty(valeurs) << " Ah, je le savais !" << std::endl;

  return 0;
}