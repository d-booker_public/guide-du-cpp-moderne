#include <array>
#include <iostream>

int main()
{
  std::array<int, 5> valeurs {1, 2, 3, 4, 5};

  std::cout << "Avant" << std::endl;
  for (auto const valeur : valeurs)
  {
    std::cout << valeur << std::endl;
  }
  
  // On remplit de 42.
  valeurs.fill(42);
  std::cout << std::endl << "Après" << std::endl;

  for (auto const valeur : valeurs)
  {
    std::cout << valeur << std::endl;
  }

  return 0;
}