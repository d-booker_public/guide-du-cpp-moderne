#include <cmath>
#include <iostream>
#include <tuple>

std::tuple<double, double, double> f(double angle)
{
  return { std::cos(angle), std::sin(angle), std::tan(angle) };
}

int main()
{
  // Seul le cosinus m'intéresse.
  double cosinus {};
  double const pi { std::acos(-1) };

  std::tie(cosinus, std::ignore, std::ignore) = f(pi);
  
  std::cout << "Cosinus de pi : " << cosinus << std::endl;
  return 0;
}