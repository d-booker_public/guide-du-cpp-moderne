#include <iostream>
#include <string>
#include <tuple>

struct Personne
{
  std::string nom;
  std::string prenom;
  int age;
};

Personne f()
{
  return { "Lagrume", "Clem", 4 };
}

int main()
{
  auto[nom, prenom, age] = f();
  std::cout << "Voici " << prenom << " " << nom << " et elle a " << age << " ans." << std::endl;

  return 0;
}