#include <cmath>
#include <iostream>
#include <string>
#include <tuple>

std::tuple<int, std::string> f()
{
  using namespace std::literals;
  return { 20, "Clem"s };
}

std::tuple<double, double> g(double angle)
{
  return { std::cos(angle), std::sin(angle) };
}

int main()
{
  // AVANT
  // std::tuple resultats_scolaires = f();
  // std::cout << "Tu t'appelles " << std::get<std::string>(resultats_scolaires) << " et tu as obtenu " << std::get<int>(resultats_scolaires) << " / 20." << std::endl;

  // Maintenant, on sait qu'on a affaire à un nom et un entier.
  auto[note, nom] = f();
  std::cout << "Tu t'appelles " << nom << " et tu as obtenu " << note << " / 20." << std::endl;

  double const pi { std::acos(-1) };
  // AVANT
  // std::tuple calculs = g(pi / 2.);
  // std::cout << "Voici le cosinus de PI / 2 : " << std::get<0>(calculs) << std::endl;
  // std::cout << "Voici le sinus de PI / 2 : " << std::get<1>(calculs) << std::endl;

  // Maintenant, on sait qu'on a affaire à deux valeurs mathématiques.
  auto[cosinus, sinus] = g(pi / 4.);
  std::cout << "Voici le cosinus de PI / 4 : " << cosinus << std::endl;
  std::cout << "Voici le sinus de PI / 4 : " << sinus << std::endl;

  return 0;
}