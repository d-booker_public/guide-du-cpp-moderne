#include <cassert>
#include <iostream>
#include <string>
#include <tuple>

int main()
{
  using namespace std::literals;

  auto bookmark = std::make_tuple("https://zestedesavoir.com"s, "Zeste de Savoir"s, 30);
  auto&[url, titre, visites] = bookmark;
  ++visites;

  assert(std::get<2>(bookmark) == 31);
  return 0;
}