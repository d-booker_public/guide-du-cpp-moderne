#include <cmath>
#include <iostream>
#include <tuple>

std::tuple<double, double, double> f(double angle)
{
  return { std::cos(angle), std::sin(angle), std::tan(angle) };
}

int main()
{
  // Création au préalable.
  double cosinus {};
  double sinus {};
  double tangente {};
  double const pi { std::acos(-1) };

  std::tie(cosinus, sinus, tangente) = f(pi);
  
  std::cout << "Cosinus de pi : " << cosinus << std::endl;
  std::cout << "Sinus de pi : " << sinus << std::endl;
  std::cout << "Tangente de pi : " << tangente << std::endl;
  
  return 0;
}