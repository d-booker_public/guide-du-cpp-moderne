#include <iostream>
#include <string>

struct Fraction
{
  int numerateur;
  int denominateur;
};

int main()
{
  Fraction const un_demi { 1, 2 };
  Fraction const trois_quarts { 3, 4 };
  Fraction const resultat { un_demi + trois_quarts };

  std::cout << "1/2 + 3/4 font " << resultat.numerateur << "/" << resultat.denominateur << std::endl;
  return 0;
}