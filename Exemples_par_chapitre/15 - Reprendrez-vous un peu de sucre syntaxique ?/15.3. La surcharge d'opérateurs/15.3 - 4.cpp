#include <iostream>
#include <string>

struct Fraction
{
  int numerateur;
  int denominateur;
};

// On vérifie que a et b sont égaux.
bool operator==(Fraction const & a, Fraction const & b)
{
  Fraction const gauche { a.numerateur * b.denominateur, a.denominateur * b.denominateur };
  Fraction const droite { b.numerateur * a.denominateur, b.denominateur * a.denominateur };
  return gauche.numerateur == droite.numerateur && gauche.denominateur == droite.denominateur;
}

// Pour aller plus vite, il suffit de
// retourner la négation de la condition 'a == b'.
bool operator!=(Fraction const & a, Fraction const & b)
{
  return !(a == b);
}

// On vérifie si a est plus petit que b.
bool operator<(Fraction const & a, Fraction const & b)
{
  if (a.numerateur == b.numerateur)
  {
    return a.denominateur > b.denominateur;
  }
  else if (a.denominateur == b.denominateur)
  {
    return a.numerateur < b.numerateur;
  }
  else
  {
    Fraction const gauche { a.numerateur * b.denominateur, a.denominateur * b.denominateur };
    Fraction const droite { b.numerateur * a.denominateur, b.denominateur * a.denominateur };
    return gauche.numerateur < droite.numerateur;
  }
}

// Pour simplifier, il suffit de vérifier
// que a n'est ni inférieur, ni égal à b.
bool operator>(Fraction const & a, Fraction const & b)
{
  return !(a < b) && a != b;
}
  
// Pour simplifier, il suffit de vérifier
// que a est inférieur ou égal à b.
bool operator<=(Fraction const & a, Fraction const & b)
{
  return a < b || a == b;
}
// Pour simplifier, il suffit de vérifier
// que a est supérieur ou égal à b.
bool operator>=(Fraction const & a, Fraction const & b)
{
  return a > b || a == b;
}
  
int main()
{
  Fraction const trois_septieme { 3, 7 };
  Fraction const un_quart { 1, 4 };

  std::cout << std::boolalpha;

  std::cout << "Est-ce que 3/7 est plus petit que 1/4 ? " << (trois_septieme < un_quart) << std::endl;
  std::cout << "Est ce que 3/7 est plus grand que 1/4 ? " << (trois_septieme > un_quart) << std::endl;

  Fraction const neuf_vingtetunieme { 9, 21 };
  std::cout << "Est-ce que 3/7 et 9/21 sont égaux ? " << (trois_septieme == neuf_vingtetunieme) << std::endl;

  return 0;
}