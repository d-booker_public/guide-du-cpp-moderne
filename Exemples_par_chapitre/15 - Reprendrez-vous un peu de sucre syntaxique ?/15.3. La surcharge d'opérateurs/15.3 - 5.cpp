#include <iostream>
#include <string>

struct Fraction
{
  int numerateur;
  int denominateur;
};

Fraction operator+(Fraction const & a, Fraction const & b)
{
  int numerateur { a.numerateur * b.denominateur + b.numerateur * a.denominateur };
  int denominateur { a.denominateur * b.denominateur };
  return { numerateur, denominateur };
}
  
std::ostream & operator<<(std::ostream & flux, Fraction const & fraction)
{
  return flux << fraction.numerateur << "/" << fraction.denominateur;
}

int main()
{
  Fraction const un_quart { 1, 4 };
  Fraction const neuf_vingtetunieme { 9, 21 };

  std::cout << "1/4 + 9/21 = " << un_quart + neuf_vingtetunieme << std::endl;
  return 0;
}