#include <iostream>
#include <string>
#include <unordered_map>

int main()
{
  using namespace std::literals;
  std::unordered_map<std::string, std::string> membres
  {
    { "mehdidou99"s, "Un auteur du cours C++."s },
    { "informaticienzero"s, "Un auteur du cours C++."s }
  };

  // Notez l'accent.
  std::string const pseudo { "informaticienzéro" };
  // ah, ça ne va pas afficher ce que vous attendiez.
  std::cout << "Qui est informaticienzero ? " << membres[pseudo] << std::endl;

  return 0;
}