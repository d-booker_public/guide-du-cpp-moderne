#include <string>
#include <unordered_map>

int main()
{
  using namespace std::literals;
  
  std::unordered_map<std::string, std::string> dictionnaire
  {
    { "Clem"s, "La mascotte du site Zeste de Savoir, toute gentille et toute mignonne."s },
    { "mehdidou99"s, "Un des auteurs du tutoriel C++."s },
    { "informaticienzero"s, "Un des auteurs du tutoriel C++."s },
    { "Taurre"s, "Un super validateur !"s },
    { "Arius"s, "Un membre super sympa mais qui mord."s },
    { "Gants de vaisselle"s, "Objets présents sur le site et dont personne ne sait pourquoi."s }
  };

  for (auto const & paire : dictionnaire)
  {
    std::cout << "Clé : " << paire.first << std::endl;
    std::cout << "Valeur : " << paire.second << std::endl << std::endl;
  }

  return 0;
}