#include <iostream>
#include <string>
#include <unordered_map>

int main()
{
  using namespace std::literals;
  
  std::unordered_map<std::string, std::string> membres
  {
    { "mehdidou99"s, "Un auteur du cours C++."s },
    { "informaticienzero"s, "Un auteur du cours C++."s }
  };
  
  // Notez l'accent.
  std::string const pseudo { "informaticienzéro" };
  if (membres.find(pseudo) != std::end(membres))
  {
    // On a trouvé le membre.
    std::cout << "Qui est informaticienzero ? " << membres[pseudo] << std::endl;
  }
  else
  {
    std::cout << "Ce membre n'existe pas.\n";
  }
  
  return 0;
}