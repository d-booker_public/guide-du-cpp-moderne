#include <iostream>
#include <string>
#include <set>

int main()
{
  std::set<std::string> const mots {
    "Archive",
    "Base",
    "Conduite",
    "Développement",
    "Entrain"
  };
  
  for (auto const & mot : mots)
  {
    std::cout << mot << std::endl;
  }

  return 0;
}