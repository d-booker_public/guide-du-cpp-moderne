#include <iostream>
#include <string>
#include <unordered_set>

int main()
{
  std::unordered_set<std::string> const mots {
    "Archive",
    "Base",
    "Conduite",
    "Développement",
    "Entrain"
  };
  
  for (auto const & mot : mots)
  {
    std::cout << mot << std::endl;
  }

  return 0;
}