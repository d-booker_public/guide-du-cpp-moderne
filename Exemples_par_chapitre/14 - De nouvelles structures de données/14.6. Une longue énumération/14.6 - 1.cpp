#include <iostream>

enum class Jour
{
  Lundi,
  Mardi,
  Mercredi,
  Jeudi,
  Vendredi,
  Samedi,
  Dimanche
};

void message(Jour jour_semaine)
{
  if (jour_semaine == Jour::Samedi || jour_semaine == Jour::Dimanche)
  {
    std::cout << "Bon week-end !" << std::endl;
  }
  else
  {
    std::cout << "Courage !" << std::endl;
  }
}

int main()
{
  Jour const aujourdhui { Jour::Mardi };
  message(aujourdhui);
  return 0;
}