#include <iostream>
#include <string>

struct InformationsPersonnelles
{
  std::string prenom;
  std::string nom;
  std::string sexe;
  int age;
};

int main()
{
  InformationsPersonnelles info_clem { "Clem", "Lagrume", "Fruit", 5 };

  // Modification.
  info_clem.age = 4;

  // Utilisation en lecture.
  std::cout << "Je m'appelle " << info_clem.prenom << " " << info_clem.nom << " et j'ai " << info_clem.age << " ans." << std::endl;

  return 0;
}