#include <iostream>
#include <string>
#include <tuple>

int main()
{
  using namespace std::literals;
  auto informations = std::make_tuple("Clem"s, "Lagrume"s, "Fruit"s, 4);

  // Accès en lecture.
  std::cout << "Prénom : " << std::get<0>(informations) << std::endl;
  std::cout << "Âge : " << std::get<3>(informations) << std::endl;

  // Accès en écriture.
  std::get<3>(informations) += 1; // Clem a maintenant 5 ans !
  std::cout << "Âge maintenant : " << std::get<3>(informations) << std::endl;

  return 0;
}