// Si on compile avec C++20, la valeur pi est fournie dans la
// bibliothèque standard avec le fichier d'en-tête <numbers>.
// Pour utiliser la valeur pi, il faut écrire une ligne spéciale,
// using namespace std::numbers;
// Ensuite on peut utiliser pi, ainsi que d'autres constantes 
// mathématiques : std::sqrt(pi) + std::pow(e, ln2), etc.
 
// Sinon, on peut s'appuyer sur la propriété que PI = acos(-1).
// double const pi { std::acos(-1) };
#include <cmath>
#include <iostream>
#include <string>
#include <tuple>

std::tuple<int, std::string> f()
{
  using namespace std::literals;
  // On retourne un std::tuple qui contient un entier
  // et une chaîne de caractères.
  return std::make_tuple(20, "Clem"s);
}

std::tuple<double, double> g(double angle)
{
  // On retourne un std::tuple qui contient deux flottants.
  // Notez la nouvelle syntaxe à base d'accolades.
  return { std::cos(angle), std::sin(angle) };
}

int main()
{
  std::tuple resultats_scolaires = f();
  std::cout << "Tu t'appelles " << std::get<std::string>(resultats_scolaires) << " et tu as obtenu " << std::get<int>(resultats_scolaires) << " / 20." << std::endl;
  
  double const pi { std::acos(-1) };
  std::tuple calculs = g(pi / 2.);
  std::cout << "Voici le cosinus de PI / 2 : " << std::get<0>(calculs) << std::endl;
  std::cout << "Voici le sinus de PI / 2 : " << std::get<1>(calculs) << std::endl;

  return 0;
}