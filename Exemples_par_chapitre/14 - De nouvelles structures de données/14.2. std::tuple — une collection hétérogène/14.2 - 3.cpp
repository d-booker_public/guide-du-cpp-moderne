#include <iostream>
#include <string>
#include <tuple>

int main()
{
  using namespace std::literals;

  auto identite = std::make_tuple("Clem"s, "Lagrume"s);
  // Ne compile pas car il y a deux éléments de même type. Comment choisir ?
  std::get<std::string>(identite);

  return 0;
}