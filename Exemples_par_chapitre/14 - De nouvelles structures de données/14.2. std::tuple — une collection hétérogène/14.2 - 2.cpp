#include <iostream>
#include <string>
#include <tuple>

int main()
{
  using namespace std::literals;

  // Prénom et salaire.
  auto informations = std::make_tuple("Clem"s, 50000);

  std::cout << "Prénom : " << std::get<std::string>(informations) << std::endl;
  std::cout << "Salaire : " << std::get<int>(informations) << "€" << std::endl;
  return 0;
}