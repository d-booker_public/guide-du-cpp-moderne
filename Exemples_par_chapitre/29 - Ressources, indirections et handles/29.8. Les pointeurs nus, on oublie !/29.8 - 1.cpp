#include <functional>
#include <iostream>

struct Bouton
{
  void onClick(std::function<void()> callback);
};

void Bouton::onClick(std::function<void()> callback)
{
  std::cout << "Reçu un évènement de clic.\n";
  callback();
}

void fonction()
{
  std::cout << "Clic sur le bouton avec une fonction libre.\n";
}

int main()
{
  Bouton bouton {};

  bouton.onClick([]() -> void {
    std::cout << "Clic sur le bouton avec une lambda.\n";
  });

  bouton.onClick(fonction);

  return 0;
}