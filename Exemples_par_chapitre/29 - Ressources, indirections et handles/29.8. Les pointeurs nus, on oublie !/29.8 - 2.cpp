#include <exception>
#include <iostream>
#include <optional>
#include <string>

std::optional<int> chaine_vers_entier(std::string const & chaine) noexcept
{
  try
  {
    return std::stoi(chaine);
  }
  catch (std::exception const & exception)
  {
    return std::nullopt;
  }
}

int main()
{
  auto const conversion_reussie { chaine_vers_entier("100") };
  std::cout << conversion_reussie.value_or(-1) << "\n";

  auto const conversion_echouee { chaine_vers_entier("ABCDEFG") };
  std::cout << conversion_echouee.value_or(-1) << "\n";

  return 0;
}