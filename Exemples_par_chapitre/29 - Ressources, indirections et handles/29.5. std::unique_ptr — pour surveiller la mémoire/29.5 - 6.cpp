#include <memory>
#include <utility>

class Moteur
{
  // ...
};

class V6 : public Moteur
{
};

class V8 : public Moteur
{
};

struct Voiture
{
  // public pour l'exemple.
  std::unique_ptr<Moteur> m_moteur;
};

void changer_moteur(Voiture& donneuse, Voiture& receveuse)
{
  receveuse.m_moteur = std::move(donneuse.m_moteur);
}

int main()
{
  // N'a plus de moteur.
  Voiture a_reparer { nullptr };

  // Le V8 de cette voiture est encore bon.
  Voiture epave { std::make_unique<V8>() };
  changer_moteur(epave, a_reparer);

  // La voiture à réparer a maintenant un V8 qui ronronne.
  // L'épave peut être broyée sans regret.

  return 0;
}