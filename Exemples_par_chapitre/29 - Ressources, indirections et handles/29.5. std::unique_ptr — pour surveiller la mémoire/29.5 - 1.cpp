#include <memory>

struct A
{
  int entier;
  double reel;
};

int main()
{
  // Sur un type natif.
  auto handle_int { std::make_unique<int>(42) };

  // Sur un type que nous avons nous-mêmes défini.
  auto handle_a { std::make_unique<A>(A { 42, 3.1415 }) };

  // En C++20, on peut même raccourcir encore l'écriture.
  // auto handle_a_cpp20 { std::make_unique<A>(42, 3.1415) };

  // La mémoire est automatiquement libérée, 'handle_a' ne gère plus rien.
  handle_int = nullptr;

  // 'handle_a' est de nouveau propriétaire d'une ressource de type 'int'.
  handle_int = std::make_unique<int>(0);

  return 0;
}
