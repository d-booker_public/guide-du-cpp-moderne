#include <string>
#include <unordered_map>

struct Texture
{
  // Toute la logique et les services
  // qu'on attend d'une texture.
};

struct Objet3D
{
  Texture* m_texture = nullptr;
};

int main()
{
  // Chargement des textures, etc.
  std::unordered_map<std::string, Texture> textures { charge_textures() };

  // Pas de texture de base, la balle est invisible.
  Objet3D balle {};

  // On utilise la texture 'balle bleue'.
  // La balle est maintenant visible.
  balle.m_texture = &textures["balle_bleue"];

  // On peut changer autant qu'on veut.
  // Il n'y a pas de risque de fuite, car 
  // 'm_texture' n'est qu'un observateur.
  balle.m_texture = &textures["balle_rouge"];
  balle.m_texture = nullptr;
  balle.m_texture = &textures["balle_verte"];

  return 0;
}