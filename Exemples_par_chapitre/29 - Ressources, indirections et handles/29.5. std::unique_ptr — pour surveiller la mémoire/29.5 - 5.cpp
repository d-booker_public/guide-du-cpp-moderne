#include <memory>

template<typename T>
using ObserverPtr = T*;

int main()
{
  // Handle propriétaire.
  auto handle_proprietaire { std::make_unique<int>(42) };

  // Observateur à l'aide d'une référence.
  int& reference { *handle_proprietaire };

  // Observateur à l'aide d'un pointeur.
  ObserverPtr<int> pointeur { handle_proprietaire.get() };

  return 0;
}