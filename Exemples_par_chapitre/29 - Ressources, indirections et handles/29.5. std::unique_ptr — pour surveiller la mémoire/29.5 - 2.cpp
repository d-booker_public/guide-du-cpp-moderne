#include <iostream>
#include <memory>

struct A
{
  void parler() const
  {
    std::cout << "Bonjour, je suis A.\n";
  }
};

int main()
{
  auto const pointeur_double { std::make_unique<double>(3.1415) };
  std::cout << *pointeur_double << std::endl;

  auto const pointeur_a { std::make_unique<A>() };
  pointeur_a ->parler();

  return 0;
}