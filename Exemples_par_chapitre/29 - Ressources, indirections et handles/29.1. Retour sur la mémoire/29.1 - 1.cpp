int main()
{
  if (true)
  {
    // La mémoire est réservée pour la variable 'age'.
    int const age { 42 };
    // Du code...
    if (age == 42)
    {
    }
    // Du code...
    // On atteint la fin de la portée,
    // la mémoire occupée par 'age' est libérée.
  }

  // Erreur, le compilateur ne connait pas la variable 'age'.
  age += 5;
  return 0;
}