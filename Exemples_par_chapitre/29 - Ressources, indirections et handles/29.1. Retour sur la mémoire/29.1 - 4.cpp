#include <iostream>

void fonction()
{
  static int compteur_statique { 0 };
  std::cout << "Adresse de 'compteur_statique' : " << &compteur_statique << "\n";
}

int main()
{
  int const variable_locale { 3 };
  // Adresse d'une variable locale.
  std::cout << "Adresse de 'variable_locale' : " << &variable_locale << "\n";

  // Les fonctions aussi ont des adresses.
  std::cout << "Adresse de 'fonction' : " << &fonction << "\n";
  fonction();

  return 0;
}