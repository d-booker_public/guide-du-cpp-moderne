#include <iostream>

void fonction()
{
  static int compteur_statique { 0 };
  int compteur { 0 };

  ++compteur_statique;
  ++compteur;

  std::cout << "Le compteur statique vaut maintenant" << compteur_statique << std::endl;
  std::cout << "Le compteur vaut maintenant " << compteur << std::endl;
}

int main()
{
  fonction();
  fonction();
  fonction();

  return 0;
}