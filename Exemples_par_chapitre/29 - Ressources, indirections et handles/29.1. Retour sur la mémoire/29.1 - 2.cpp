int& fonction()
{
  int variable_locale { 4 };
  int& reference_variable_locale { variable_locale };
  // Erreur, 'variable_locale' n'existera plus.
  // La référence ne sera plus valide.
  return reference_variable_locale;
}

int main()
{
  // Référence sur n'importe quoi.
  int& reference { fonction() };
  return 0;
}