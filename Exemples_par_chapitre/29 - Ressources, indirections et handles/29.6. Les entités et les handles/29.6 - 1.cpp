#include <iostream>
#include <memory>
#include <string>
#include <vector>

class Personne
{
public:
  Personne() = delete;
  ~Personne() noexcept = default;
  Personne(std::string const& prenom, std::string const& nom, int salaire)
    : m_prenom(prenom), m_nom(nom), m_salaire(salaire)
  {
  }

  Personne(Personne const& copie) = delete;
  Personne& operator=(Personne const& autre_personne) = delete;

  virtual void travailler() const = 0;

private:
  std::string m_nom;
  std::string m_prenom;
  int m_salaire;
};

class Directeur : public Personne
{
public:
  using Personne::Personne;
  ~Directeur() noexcept = default;
  Directeur(std::string const& prenom, std::string const& nom, int salaire, std::string const& service)
    : Personne(prenom, nom, salaire), m_service(service)
  {
  }

  void travailler() const override
  {
    std::cout << "J'assiste à des réunions.\n";
  }

private:
  std::string m_service;
};

enum class EquipementProctection
{
  Gants,
  Casque,
  Chassures,
  Lunettes,
  Masque
};

using EquipementsProtection = std::vector<EquipementProctection>;

class Ouvrier : public Personne
{
public:
  using Personne::Personne;

  ~Ouvrier() noexcept = default;
  Ouvrier(std::string const& prenom, std::string const& nom, int salaire, EquipementsProtection const& epi)
    : Personne(prenom, nom, salaire), m_epi(epi)
  {
  }

  void travailler() const override
  {
    std::cout << "Je travaille avec mes outils.\n";
  }

private:
  EquipementsProtection m_epi;
};

using Entreprise = std::vector<std::unique_ptr<Personne>>;

int main()
{
  std::cout << sizeof(Personne) << std::endl;
  std::cout << sizeof(Directeur) << std::endl;
  std::cout << sizeof(Ouvrier) << std::endl;

  Entreprise usine {};

  usine.push_back(std::make_unique<Directeur>(
    "Eting",
    "Mark",
    8000,
    "Marketing"
  ));

  std::vector<EquipementProctection> const epi_soudeur {
    EquipementProctection::Gants,
    EquipementProctection::Casque,
    EquipementProctection::Lunettes,
    EquipementProctection::Chassures
  };

  usine.push_back(std::make_unique<Ouvrier>(
    "Le soudeur fou",
    "Bob",
    2300,
    epi_soudeur 
  ));

  // Nous sommes en mesure d'avoir une collection d'entités polymorphiques.
  for (auto const& personne : usine)
  {
    std::cout << "\n";
    personne->travailler();
  }

  return 0;
}