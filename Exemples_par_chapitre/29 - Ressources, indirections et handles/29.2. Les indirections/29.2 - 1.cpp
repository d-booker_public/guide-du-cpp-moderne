#include <iostream>

int main()
{
  int variable_modifiable { 3 };

  // Le pointeur est maintenant initialisé avec l'adresse de 'variable_modifiable'.
  int* pointeur { &variable_modifiable };
  std::cout << "Adresse de 'variable_modifiable' : " << &variable_modifiable << "\n";
  std::cout << "Valeur du pointeur : " << pointeur << "\n";

  // Déréférencement pour accéder à la valeur pointée.
  std::cout << "Valeur pointée : " << *pointeur << "\n";

  // Comme pour une référence modifiable, on peut changer la valeur derrière l'indirection.
  *pointeur = 98;
  std::cout << "variable_modifiable : " << variable_modifiable << "\n";
  std::cout << "Valeur pointée : " << *pointeur << "\n";

  int autre_variable { -7 };
  // Contrairement à une référence qui est permanente, on peut changer la valeur sur laquelle pointer.
  pointeur = &autre_variable;
  std::cout << "Valeur pointée après modification : " << *pointeur << "\n";

  return 0;
}