#include <iostream>
#include <memory>

class A
{
public:
  ~A() noexcept;
};

A::~A() noexcept
{
  std::cout << "Destructeur de A non-virtuel.\n";
}

class B : public A
{
public:
  ~B() noexcept;
};

B::~B() noexcept
{
  std::cout << "Destructeur de B non-appelé.\n";
}

class C
{
public:
  virtual ~C() noexcept;
};

C::~C() noexcept
{
  std::cout << "Destructeur de C virtuel.\n";
}

class D : public C
{
public:
  ~D() noexcept override;
};

D::~D() noexcept
{
  std::cout << "Destructeur de D appelé.\n";
}

int main()
{
  // Comportement indéterminé au moment de la libération car partielle.
  std::unique_ptr<A> const objet_a {std::make_unique<B>()};

  // Libération complète et correcte.
  std::unique_ptr<C> const objet_c {std::make_unique<D>()};

  return 0;
}