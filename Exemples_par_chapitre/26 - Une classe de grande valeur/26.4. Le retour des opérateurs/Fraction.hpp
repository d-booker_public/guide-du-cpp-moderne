#ifndef FRACTION_HPP
#define FRACTION_HPP

#include <istream>
#include <ostream>
#include <string>

class Fraction
{
public:
  Fraction(int numerateur, int denominateur);
  Fraction(std::string const & chaine);

  friend std::ostream& operator<<(std::ostream & os, Fraction const & fraction);
  friend std::istream& operator>>(std::istream & is, Fraction & fraction);

private:
  int m_numerateur { 1 };
  int m_denominateur { 1 };
};

#endif