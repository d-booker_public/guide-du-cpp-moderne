#include "Fraction.hpp"
#include <iostream>

template<typename T>
void entree_securisee(T & variable)
{
  while (!(std::cin >> variable))
  {
    if (std::cin.eof())
    {
      break;
    }
    else if (std::cin.fail())
    {
      std::cout << "Entrée invalide. Recommence." << std::endl;
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    else
    {
      std::cout << "Le prédicat n'est pas respecté !" << std::endl;
    }
  }
}

int main()
{
  Fraction const f1 { "5/2" };
  std::cout << f1 << "\n";

  Fraction f2 {};
  // Grâce à notre écriture de operator>> conforme aux normes, on peut
  // réutiliser la fonction entree_securisee sans plus d'effort.
  entree_securisee(f2);
  std::cout << f2 << "\n";

  return 0;
}