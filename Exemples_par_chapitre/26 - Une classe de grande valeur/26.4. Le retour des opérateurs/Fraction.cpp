#include "Fraction.hpp"

#include <algorithm>
#include <cassert>
#include <exception>

/**
 * @brief Extrait le numérateur et le dénominateur d'une chaîne, si possible.
 * @param[in] chaine La chaîne de caractères à analyser.
 * @param[out] numerateur Le numérateur extrait.
 * @param[out] denominateur Le dénominateur extrait.
 * @exception std::runtime_error si la chaîne n'est pas correcte.
 */
void extraction_depuis_chaine(std::string const & chaine, int & numerateur, int & denominateur)
{
  if (std::empty(chaine))
  {
    throw std::runtime_error("La chaîne ne peut pas être vide.");
  }

  auto const debut = std::begin(chaine);
  auto const fin = std::end(chaine);
  if (std::count(debut, fin, '/') != 1)
  {
    throw std::runtime_error("La chaîne ne peut contenir qu'un seul séparateur '/'.");
  }

  auto iterateur = std::find(debut, fin, '/');
  // Récupération du numérateur.
  std::string const n { debut, debut + std::distance(debut, iterateur) };
  if (std::empty(n))
  {
    throw std::runtime_error("Le numérateur ne peut pas être vide.");
  }

  // Pour sauter le caractère /.
  ++iterateur;
  // Récupération du dénominateur.
  std::string const d { iterateur, fin };
  if (std::empty(d) || std::stoi(d) == 0)
  {
    throw std::runtime_error("La dénominateur ne peut pas être vide ou nul.");
  }

  // Mise à jour uniquement si tout s'est bien passé.
  numerateur = std::stoi(n);
  denominateur = std::stoi(d);
}

Fraction::Fraction(int numerateur, int denominateur)
  : m_numerateur(numerateur), m_denominateur(denominateur)
{
  assert(denominateur != 0 && "Le dénominateur doit toujours être différent de 0.");
}

Fraction::Fraction(std::string const & origine)
{
  extraction_depuis_chaine(origine, m_numerateur, m_denominateur);
}

// Rien de particulier, le code est simple.
std::ostream& operator<<(std::ostream & os, Fraction const & fraction)
{
  os << fraction.m_numerateur << "/" << fraction.m_denominateur;
  return os;
}
// Ici, on a un peu plus de travail.
std::istream& operator>>(std::istream & is, Fraction & fraction)
{
  std::string entree {};
  is >> entree;
  try
  {
    int numerateur { 0 };
    int denominateur { 0 };
    extraction_depuis_chaine(entree, numerateur, denominateur);
    // Si on arrive ici, tout va bien, on peut créer le nouvelle objet.
    fraction = Fraction { numerateur, denominateur };
  }
  catch (std::exception const & e)
  {
    // On attrape n'importe quelle exception, que ce soit parce
    // que la chaîne est invalide ou parce que stoi a échoué
    // lors de la conversion.
    is.setstate(std::ios::failbit);
  }
  
  return is;
}