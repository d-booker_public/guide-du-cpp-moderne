class Fraction
{
public:
  Fraction& operator+=(Fraction const & origine) noexcept;

private:
  int m_numerateur { 0 };
  int m_denominateur { 1 };
};

Fraction& Fraction::operator+=(Fraction const & origine) noexcept
{
  m_numerateur = m_numerateur * origine.m_denominateur + origine.m_numerateur * m_denominateur;
  m_denominateur *= origine.m_denominateur;
  return *this;
}

// L'argument lhs est recopié, donc on peut le modifier sans problème.
Fraction operator+(Fraction lhs, Fraction const & rhs) noexcept
{
  lhs += rhs;
  return lhs;
}