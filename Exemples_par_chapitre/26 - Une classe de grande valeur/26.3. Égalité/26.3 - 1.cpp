#include <cassert>
#include <iostream>
#include <numeric>

class Fraction
{
public:
  // Constructeur de conversion car un seul paramètre obligatoire. 
  Fraction(int numerateur, int denominateur = 1) noexcept;
  void simplifier();
  // Version membre.
  bool operator==(Fraction const & rhs) const noexcept;

private:
  int m_numerateur { 0 };
  int m_denominateur { 1 };
};

Fraction::Fraction(int numerateur, int denominateur) noexcept
  : m_numerateur(numerateur), m_denominateur(denominateur)
{
  assert(denominateur != 0 && "Le dénominateur ne peut pas valoir 0.");
  simplifier();
}

// Implémentation de la version membre.
bool Fraction::operator==(Fraction const & rhs) const noexcept
{
  return m_numerateur == rhs.m_numerateur && m_denominateur == rhs.m_denominateur;
}

void Fraction::simplifier()
{
  int const pgcd { std::gcd(m_numerateur, m_denominateur) };
  m_numerateur /= pgcd;
  m_denominateur /= pgcd;
}

int main()
{
  Fraction const fraction { 4, 2 };
  int const entier { 2 };
  if (entier == fraction)
  {
    std::cout << "4/2 est égal à 2.\n";
  }
  else
  {
    std::cout << "4/2 est différent de 2.\n";
  }

  return 0;
}