# Le Guide du C++ moderne

Vous trouverez sur ce dépôt les ressources liées au livre [Le Guide du C++ moderne – de débutant à développeur](https://www.d-booker.fr/cppmoderne/656-le-guide-du-c-moderne-de-debutant-a-developpeur.html), écrit par Benoît Vittupier et Mehdi Benharrats, paru aux éditions D-BookeR le 19/08/2021.

![Couverture du livre](livre_guide_cpp_moderne.png)


**Sommaire :**

0. Bienvenue !

1. C'est décidé, je m'y mets !

I. LE DÉBUT DU VOYAGE

    2. Le minimum pour commencer
    3. Rencontre avec le C⁠+⁠+
    4. Tout ça est bien variable
    5. Le conditionnel conjugué en C⁠+⁠+
    6. Des boucles qui se répètent, répètent, répètent...
    7. Au tableau !
    8. Déployons la toute puissance des conteneurs
    9. Des flux dans tous les sens

II. ON PASSE LA DEUXIÈME !

    10. Découpons le code — les fonctions
    11. Erreur, erreur, erreur…
    12. Des fonctions somme toute lambdas
    13. Envoyez le générique !
    14. De nouvelles structures de données
    15. Reprendrez-vous un peu de sucre syntaxique ?
    16. [Pratique] Un gestionnaire de discographie
    17. Découpons du code — les fichiers

III. INTERLUDE : ÊTRE DÉVELOPPEUR

    18. Un coup d'œil dans le rétro
    19. Mais où est la doc ?
    20. Compilation en cours...
    21. Chasse aux bugs !
    22. Une foule de bibliothèques
    23. Améliorer ses projets

IV. LA PROGRAMMATION ORIENTÉE OBJET

    24. Premiers pas avec la POO
    25. Et qui va construire tout ça ?
    26. Une classe de grande valeur
    27. [Pratique] Entrons dans la matrice
    28. Classes à sémantique d'entités
    29. Ressources, indirections et handles
    30. La sémantique de déplacement
    31. Oh, le bel héritage
    32. Les classes templates
    33. Ça, c'est du SOLID !

34. Le voyage ne fait que commencer





