#ifndef MATRICE_HPP
#define MATRICE_HPP

#include <iosfwd>
#include <vector>

/**
* @brief Manipulation de matrices.
*/
class Matrice
{
public:
  /**
    * @brief Créé une nouvelle instance de Matrice.
    * @param[in] nb_lignes Le nombre de lignes de la matrice à créer.
    * @param[in] nb_colonnes Le nombre de colonnes de la matrice à créer.
    * @pre Le nombre de lignes et de colonnes doit être strictement supérieur à 0.
    * @param[in] valeur_initiale La valeur à donner à chaque élément de la matrice.
    */
  Matrice(std::size_t nb_lignes, std::size_t nb_colonnes, int valeur_initiale = 0);

  /**
    * @brief Constructeur de copie.
    * @param[in] copie La matrice à copier.
    */
  Matrice(Matrice const & copie) = default;

  /**
    * @brief Opérateur d'affectation par copie.
    * @param[in] autre_matrice La matrice à copier.
    * @return La matrice actuelle après copie.
    */
  Matrice& operator=(Matrice const & autre_matrice) = default;

  /**
    * @brief Opérateur d'accès en lecture seule à un élément de la matrice.
    * @param[in] x L'indice de la ligne voulue.
    * @param[in] y L'indice de la colonne voulue.
    * @pre Les indices de ligne et de colonne doivent être strictement inférieurs à la dimension de la matrice.
    * @return Une référence constante sur l'élément trouvé.
    */
  int const & operator()(std::size_t x, std::size_t y) const;

  /**
    * @brief Opérateur d'accès en lecture/écriture à un élément de la matrice.
    * @param[in] x L'indice de la ligne voulue.
    * @param[in] y L'indice de la colonne voulue.
    * @pre Les indices de ligne et de colonne doivent être strictement inférieurs à la dimension de la matrice.
    * @return Une référence sur l'élément trouvé.
    */
  int& operator()(std::size_t x, std::size_t y);

  /**
    * @brief Additionne une matrice à la matrice courante.
    * @param autre_matrice La matrice à additionner à la matrice courante.
    * @pre La matrice passée en paramètre doit être de même dimension que la matrice courante.
    * @post La nouvelle valeur est égale à l'ancienne plus `rhs`
    * @return Matrice& L'objet modifié après addition.
    */
  Matrice& operator+=(Matrice const & autre_matrice);

  /**
    * @brief Opérateur d'addition de deux matrices.
    * @param[in] lhs La matrice de gauche. 
    * @param[in] rhs La matrice de droite.
    * @pre Les deux matrices doivent être de même dimension.
    * @return Le résultat de l'addition des deux matrices.
    */
  friend Matrice operator+(Matrice lhs, Matrice const & rhs);

  /**
    * @brief Multiplie la matrice courante par un entier.
    * @param[in] multiplicateur Un entier, par lequel la matrice est multipliée.
    * @return L'objet modifié après multiplication.
    */
  Matrice& operator*=(int multiplicateur);

  /**
    * @brief Opérateur de multiplication d'une matrice par un entier.
    * @param[in] m La matrice à multiplier.
    * @param[in] multiplicateur L'entier multiplicateur.
    * @return Le résultat de la multiplication d'une matrice par un entier.
    */
  friend Matrice operator*(Matrice m, int multiplicateur);

  /**
    * @brief Opérateur de multiplication d'une matrice par un entier.
    * @param[in] multiplicateur L'entier multiplicateur.
    * @param[in] m La matrice à multiplier.
    * @return Le résultat de la multiplication d'une matrice par un entier.
    */
  friend Matrice operator*(int multiplicateur, Matrice m);

  /**
    * @brief Multiplie la matrice courante par une autre matrice.
    * @param[in] autre_matrice La matrice par laquelle la matrice courante sera multipliée.
    * @return L'objet modifié après multiplication.
    */
  Matrice& operator*=(Matrice const & autre_matrice);

  /**
    * @brief Opérateur de multiplications de matrices.
    * @param[in] lhs La matrice de gauche.
    * @param[in] rhs La matrice de droite.
    * @return Le résultat de la multiplication d'une matrice par une autre.
    */
  friend Matrice operator*(Matrice const & lhs, Matrice const & rhs);

  /**
    * @brief Affiche une matrice sur un flux de sortie.
    * @param[in,out] stream Le flux sur lequel sera affichée la matrice.
    * @param[in] matrice La matrice à afficher.
    * @return Le flux de sortie utilisé.
    */
  friend std::ostream& operator<<(std::ostream & stream, Matrice const & matrice);
  
  /**
    * @brief Retourne un vecteur ligne, selon l'indice demandé.
    * @param[in] index_ligne L'index de la ligne désirée.
    * @pre L'index de la ligne désirée doit être strictement inférieur au nombre de lignes de la matrice.
    * @return Un vecteur ligne de type Matrice, correspondant à la ligne demandée.
    */
  Matrice ligne(std::size_t index_ligne) const;

  /**
    * @brief Retourne un vecteur colonne, selon l'indice demandé.
    * @param[in] index_colonne L'index de la colonne désirée.
    * @pre L'index de la colonne désirée doit être strictement inférieur au nombre de colonnes de la matrice.
    * @return Un vecteur colonne de type Matrice, correspondant à la colonne demandée.
    */
  Matrice colonne(std::size_t index_colonne) const;

  /**
    * @brief Obtient la transposée de la matrice courante.
    * @return La matrice courante une fois transposée.
    */
  Matrice transpose() const;

  /**
    * @brief Donne le nombre de lignes de la matrice actuelle.
    * @return Le nombre de lignes de la matrice actuelle.
    */
  std::size_t nb_lignes() const noexcept;

  /**
    * @brief Donne le nombre de colonnes de la matrice actuelle.
    * @return Le nombre de colonnes de la matrice actuelle.
    */
  std::size_t nb_colonnes() const noexcept;

private:
  /**
    * Permet de calculer la position dans le tableau interne de l'élement qui se trouve à la ligne et à la colonne demandées.
    * @param[in] ligne La ligne demandée.
    * @param[in] colonne La colonne demandée.
    * @pre La ligne et la colonne doivent être inférieures à la dimension de la matrice.
    * @returns L'index dans le tableau auquel se trouve l'élément.
    */ 

  std::size_t offset(std::size_t ligne, std::size_t colonne) const noexcept;
  std::size_t m_nb_lignes;
  std::size_t m_nb_colonnes;
  std::vector<int> m_tableau_interne;
};

#endif